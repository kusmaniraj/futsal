<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_courts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booked_id');
            $table->integer('user_id')->unsigned();
            $table->string('note');
            $table->enum('deleteType',[0,1])->default(0);;

            $table->enum('status', ['Pending', 'Confirmed','Not_Confirmed'])->default('Pending');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_courts');
    }
}
