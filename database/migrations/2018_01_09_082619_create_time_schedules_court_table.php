<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSchedulesCourtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_schedules_court', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('date')->nullable();
            $table->integer('court_id')->unsigned()->nullable();
            $table->foreign('court_id')->references('id')
                ->on('courts')->onDelete('cascade');

            $table->integer('time_schedule_id')->unsigned()->nullable();
            $table->foreign('time_schedule_id')->references('id')
                ->on('time_schedules')->onDelete('cascade');
//            $table->date('booking_date')->nullable();

            $table->enum('status', ['booked', 'available','reservedBooked'])->default('available');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_schedules_court');
    }
}
