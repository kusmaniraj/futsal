<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourtScheduleCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('court_schedule_courts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('court_id')->unsigned()->nullable();
            $table->foreign('court_id')->references('id')
                ->on('courts')->onDelete('cascade');;

            $table->integer('court_schedule_id')->unsigned()->nullable();
            $table->foreign('court_schedule_id')->references('id')
                ->on('court_schedules')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('court_schedule_courts');
    }
}
