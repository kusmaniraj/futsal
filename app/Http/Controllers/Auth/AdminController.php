<?php

namespace App\Http\Controllers\Auth;
use App\Traits\Shared;

use App\Admin;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use Shared;
    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits


    }


    public function index()
    {

        return view('admin.dashboard',$this->data);
    }

    public function showLoginForm(){
        $this->data['title']='Login';

        return view('auth.admin.login',$this->data);
    }

    public function login(Request $request)
    {



        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('admin.dashboard'));
        }else{
            $errors['email']='These credentials do not match our records.';
            // if unsuccessful, then redirect back to the login with the form data
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors($errors);
        }

    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
    public function showRegisterForm(){
        $this->data['title']='Register';
        return view('auth.admin.register',$this->data);
    }
    public function register(Request $request){
        $data=$request->all();
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $result=Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        if($result){
            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {

                return redirect()->intended(route('admin.dashboard'));
            }
        }

    }
}
