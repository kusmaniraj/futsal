<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Traits\Shared;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use Shared;
    private $data;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits
        $this->middleware('guest')->except('logout');
        $this->data['title']='Login';
    }
    public function showLoginForm()
    {
        return view('auth.login',$this->data);
    }
    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
