<?php

namespace App\Http\Controllers\web;

use App\BookingCourt;
use App\Http\Controllers\Admin\SettingController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\User;
use Hash;
use App\Traits\Shared;

class AccountSettingController extends Controller
{
    private $data;
    use Shared;


    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits
        $this->data['title'] = 'Account Setting';


    }

    public function showAccountSettingForm()
    {
        if (Auth::guard('web')->check()) {
            $this->data['countBooking'] = BookingCourt::where('user_id', Auth::user()->id)->count();
        }
        $this->data['user'] = Auth::user();
        return view('web/accountSetting', $this->data);
    }

    public function updateAccountSetting(Request $request)
    {
        $user = Auth::user();


        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'unique:users,email,'.$user->id.'|required|string|email|max:255',
            'c_password'=>'required'
        ],['c_password.required'=>'CurrentSSS Password is Required']);
        $formData = $request->all();
        $id = $formData['id'];
        if (Hash::check($formData['c_password'], $user->password)) {
            if ($formData['password']) {
                $this->validate($request, [
                    'password' => 'string|min:6|confirmed',
                ]);
                User::find($id)->update([
                    'name' => $formData['name'],
                    'email' => $formData['email'],
                    'password' => bcrypt($formData['password']),
                ]);



            } else {



                User::find($id)->update([
                    'name' => $formData['name'],
                    'email' => $formData['email'],

                ]);



            }
            Session::flash('success', 'Update Account');


        } else {
            Session::flash('c_password', 'Current Password not Match');
        }




        return redirect()->back();
    }
}
