<?php

namespace App\Http\Controllers\web;

use App\BookingCourt;
use App\CourtSchedule;
use App\Http\Controllers\Admin\SettingController;
use App\Mail\UserBookingMail;
use App\Mail\UserUnbookingMail;
use App\Time_schedule;
use App\Time_schedule_court;
use App\UserActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;
use Mail;
use Validator;
use User;


class BookingController extends Controller
{
    private $data;


    public function __construct()
    {
        $setting = new SettingController();
        $this->data['getSetting'] = $setting->getSetting();
        $this->data['title'] = 'My Booking Court';
        //            check ajax request
        if (Auth::guard('web')->check()) {
            $this->data['countBooking'] = BookingCourt::where('user_id', Auth::user()->id)->count();
        }


    }

    public function bookingCourtList()

    {

        $this->data['bookingCourtLists'] = $this->userReservationList('new');
        return view('web/bookingCourt/list', $this->data);
    }


    public function bookingCourt(Request $request)
    {
        $validater = Validator::make($request->all(), ['note' => 'required|max:255', 'terms' => 'required'], ['note.required' => 'Please Write Some Note !', 'terms.required' => 'Accept Terms for Court Booking !']);
        if ($validater->fails()) {
            return ['error' => $validater->errors()];
        }
        if (Auth::guard('web')->check()) {
            $formInput = $request->all();
            $time_schedule_courts = Time_schedule_court::all();
            $result = false;
            $formInput['status'] = 'booked';
            $booked_id = implode(",", $formInput['book_id']);
//            insert Reserve Court
            $insertReserveCourt=BookingCourt::create(['user_id' => Auth::user()->id, 'booked_id' => $booked_id, 'note' => $formInput['note'],'status'=>'confirmed']);
//            send Notification To Admin
            $userActivityData = ['user_id' => Auth::user()->id, 'booked_id' => $booked_id, 'type' => 'Booked'];
            $result = UserActivity::create($userActivityData);
            for ($i = 0; $i < count($formInput['book_id']); $i++) {

                foreach ($time_schedule_courts as $time_schedule_court) {
                    if ($formInput['book_id'][$i] == $time_schedule_court['id']) {


                        Time_schedule_court::find($formInput['book_id'][$i])->update($formInput);


                        $result = true;


                    }

                }

            }


            if ($result) {
                $date = $formInput['date'];
                $this->selectCourtSchedule($date);
//                send mail to user
                $user =Auth::user();
                Mail::to($user)->send(new UserBookingMail($user, $insertReserveCourt->id));

                if (Mail::failures()) {
                    return ['error' => 'Cannot Confirmed Booked ,Mail has not been Sent'];
                }
                $this->data['successMsg'] = ' Court is Booked';


                return view('web.ajaxSelectCourtSchedule', $this->data);

            } else {
                return response(['error' => 'UnBooked Court']);
            }


        } else {
            if ($request->ajax()) {
                return response()->view('auth.ajaxLoginForm');
            }
        }


    }


    public function removeCourt($id)
    {
        $user = Auth::user();

        $result = BookingCourt::where(['user_id' => $user->id, 'id' => $id])->update(['deleteType' => '1']);


        if ($result) {

            return ['success' => 'Remove  Court'];
        } else {
            return ['error' => ' Cannot Remove  Court'];
        }
        return redirect()->back();

    }

    function selectCourtSchedule($date)
    {


        $court_schedule['date'] = $date;

        $countCourt_schedule = CourtSchedule::with('courts')->where('date', $date)->count();
        if ($countCourt_schedule > 0) {
            $court_schedule = CourtSchedule::with('courts')->where('date', $date)->first()->toArray();
            $this->data['time_schedules_price'] = Time_schedule::where('court_schedule_id', $court_schedule['id'])->get()->toArray();
            foreach ($court_schedule['courts'] as $i => $court) {
                $court_schedule['courts'][$i]['time_schedule_courts'] = [];
                $time_schedule_courts = Time_schedule_court::where(['court_id' => $court['id'], 'date' => $date])->get()->toArray();

                foreach ($time_schedule_courts as $time_schedule_court) {
                    $array_time_schedule_court = ['status' => $time_schedule_court['status'],
                        'court_id' => $time_schedule_court['court_id'],
                        'time_schedule_id' => $time_schedule_court['time_schedule_id'],
                        'id' => $time_schedule_court['id']];
                    array_push($court_schedule['courts'][$i]['time_schedule_courts'], $array_time_schedule_court);


                }

            }
        }


        $this->data['court_schedule'] = $court_schedule;


//
//        echo'<pre>';
//        print_r($this->data);
//        echo '</pre>';
//        die();
        return $this->data;
    }

    public function userReservationList($type)
    {
        $date = date('Y-m-d') . '&nbsp00:00:00';

        $this->data['countBooking'] = BookingCourt::where(['user_id' => Auth::user()->id])->count();
        if ($type == 'new') {

            $bookingCourtLists = BookingCourt::where('user_id', Auth::user()->id)->where('updated_at', '>=', $date)->where('deleteType', '0')->orderBy('updated_at', 'desc')->get();
        } else {
            $bookingCourtLists = BookingCourt::where('user_id', Auth::user()->id)->where('updated_at', '<=', $date)->where('deleteType', '0')->orderBy('updated_at', 'desc')->get();
        }

        foreach ($bookingCourtLists as $key => $bookingCourtList) {
            $booked_id = explode(',', $bookingCourtList['booked_id']);
            $courtNames = [];
            $total_price = 0;
            $times = [];
            $dates = [];

            for ($i = 0; $i < count($booked_id); $i++) {
                $bookingList = Time_schedule_court::with('court', 'time_schedule')->where('id', $booked_id[$i])->first();
                array_push($courtNames, $bookingList['court']['court_name']);
                $total_price += $bookingList['time_schedule']['price'];
                array_push($times, date('g:i A', strtotime($bookingList['time_schedule']['start_hour'])) . '-' . date('g:i A', strtotime($bookingList['time_schedule']['end_hour'])));
                array_push($dates, $bookingList['date']);


            }
            $bookingCourtLists[$key]['courtNames'] = $courtNames;
            $bookingCourtLists[$key]['total_price'] = $total_price;
            $bookingCourtLists[$key]['dates'] = $dates;
            $bookingCourtLists[$key]['times'] = $times;


        }
        return $bookingCourtLists;
    }

    public function userReservationTypeList($type)
    {
        $this->data['bookingCourtLists'] = $this->userReservationList($type);
        return view('web.bookingCourt/ajaxUserBookingList', $this->data);

    }

    public function showUserCourtBooked($id)
    {
        $bookedCourts = BookingCourt::with('user')->find($id)->toArray();

        $total_price = 0;

        $bookedCourts['courtTimeSchedules'] = [];
        $users_booked_id = explode(',', $bookedCourts['booked_id']);
        for ($i = 0; $i < count($users_booked_id); $i++) {

            $bookingCourt = Time_schedule_court::with('court', 'time_schedule')->where('id', $users_booked_id[$i])->first()->toArray();
            array_push($bookedCourts['courtTimeSchedules'], $bookingCourt);
            $total_price += $bookingCourt['time_schedule']['price'];

        }

        $bookedCourts['total_price'] = $total_price;
//        echo "<pre>";
//        print_r($bookedCourts);
//        echo "</pre>";
        return $bookedCourts;

    }


}
