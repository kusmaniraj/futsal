<?php

namespace App\Http\Controllers\Web;

use App\BookingCourt;
use App\Court;
use App\CourtSchedule;
use App\Http\Controllers\Admin\SettingController;
use App\Mail\UserBookingMail;
use App\Time_schedule;
use App\Time_schedule_court;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mail;
use App\Traits\Shared;

class HomeController extends Controller
{
    private $data;
    use Shared;



    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits


    }

    public function index()
    {




        $this->data['title'] = "Home";
        $date = date('Y-m-d');



        $this->selectCourtSchedule($date);
//        echo "<pre>";
//        print_r($court_schedules);
//        echo "</pre>";
//        die();

        return view('web.index', $this->data);
    }

    public function viewCourt()
    {
        $this->data['title'] = "Booking Court";
        return view('web.viewCourt', $this->data);
    }



    public function ajax_selected_court(Request $request, $id)
    {

        $time_schedule_courts = Time_schedule_court::with('court', 'time_schedule')->find($id);
        $start_hour = date('g:i A', strtotime($time_schedule_courts['time_schedule']['start_hour']));
        $end_hour = date('g:i A', strtotime($time_schedule_courts['time_schedule']['end_hour']));
        $time_schedule_courts['time'] = $start_hour . '-' . $end_hour;
        $time_schedule_courts['price'] = $time_schedule_courts['time_schedule']['price'];
        $time_schedule_courts['user']=[];
        if(Auth::guard('web')->check()){
            $time_schedule_courts['user']=User::find( Auth::user()->id);
        }


//       Time_schedule_court::find($id)->update(['status'=>'unfinished_booked']);



        return $time_schedule_courts;
    }


    function selectCourtSchedule($date){


        $court_schedule['date']=$date;

        $countCourt_schedule = CourtSchedule::with('courts')->where('date', $date)->count();
        if($countCourt_schedule >0){
            $court_schedule=CourtSchedule::with('courts')->where('date', $date)->first()->toArray();
            $this->data['time_schedules_price']=Time_schedule::where('court_schedule_id',$court_schedule['id'])->get()->toArray();
            foreach ($court_schedule['courts'] as $i => $court) {
                $court_schedule['courts'][$i]['time_schedule_courts'] = [];
                $time_schedule_courts = Time_schedule_court::with('time_schedule')->where(['court_id'=>$court['id'],'date'=>$date])->get()->toArray();

                foreach ($time_schedule_courts as $time_schedule_court) {
                    $array_time_schedule_court = ['status' => $time_schedule_court['status'],
                        'court_id' => $time_schedule_court['court_id'],
                        'time_schedule_id' => $time_schedule_court['time_schedule_id'],
                        'id'=>$time_schedule_court['id'],
                        'price'=>$time_schedule_court['time_schedule']['price']

                    ];
                    array_push($court_schedule['courts'][$i]['time_schedule_courts'],$array_time_schedule_court);


                }

            }
        }




             $this->data['court_schedule']=$court_schedule;




//        echo'<pre>';
//        print_r( $this->data['court_schedule']);
//        echo '</pre>';
//        die();
        return $this->data;
    }
    function ajaxSelectCourtSchedule($date){
        $this->data['court_schedules']=$this->selectCourtSchedule($date);
        return view('web/ajaxSelectCourtSchedule',$this->data);
    }
    public function ajaxLogin(Request $request)
    {



        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // Attempt to log the user in
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return response('success Login');
        }else{
            $data['errors']=['email'=>'These credentials do not match our records.'];
            // if unsuccessful, then redirect back to the login with the form data
            return response($data);
        }

    }


}