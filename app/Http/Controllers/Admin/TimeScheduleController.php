<?php

namespace App\Http\Controllers\Admin;

use App\Time_schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Traits\Shared;

class TimeScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $data;
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
        //end Traits
        $this->data['title'] = 'Time Schedule';

    }
    public function index()
    {

        $start_hour='';
        $end_hour='';
        $timeSchedules=Time_schedule::all();
        foreach($timeSchedules as $i=> $time_schedule){
            if($i==0){
                $start_hour=$time_schedule['start_hour'];
            }

            $last_key=(count($timeSchedules)-1);
            if($i==$last_key){
                $end_hour=$time_schedule['end_hour'];
            }
            $this->data['start_hour']= $start_hour;
            $this->data['end_hour']= $end_hour;




        }

        return view('admin.time_schedule.list', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'start_hour' => 'required',
            'end_hour' => 'required|greater_than_field:start_hour',
        ],['end_hour.greater_than_field'=>'End Hour must be Greater than Start hour']);
        $formInput = $request->all();



        DB::table('time_schedules')->delete();

        for ($i = $formInput['start_hour']; $i < $formInput['end_hour']; $i++) {


           Time_schedule::create(['start_hour'=>$i.":00",'end_hour'=>($i+1).':00']);

        }

        return redirect('admin/time_schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
