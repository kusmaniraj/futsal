<?php

namespace App\Http\Controllers\Admin;

use App\Time_schedule_court;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
class CourtBookingController extends Controller
{
    use Shared;
    private $data;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
         //end Traits
        $this->data['title'] = 'Court Booking';


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['courtBookings']=Time_schedule_court::with(['court','time_schedule'])->get();

        return view('admin.courtBooking.list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
