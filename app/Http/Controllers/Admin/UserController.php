<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Mail\UserBookingMail;
use App\Mail\UserUnbookingMail;
use App\Time_schedule_court;
use App\Traits\Shared;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Mail;


class UserController extends Controller
{
    private $data;
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
        //end Traits

        $this->data['title'] = 'Users';

    }

    public function usersList()
    {
        $this->data['users'] = User::all();
        return view('admin.users.list', $this->data);
    }

    public function delete($id)
    {
        $result = User::find($id)->delete();
        if ($result) {
            Session::flash('success', 'User is Deleted');
        } else {
            Session::flash('error', 'User is Deleted');
        }
        return redirect()->back();
    }

    public function usersCourtsReservation()
    {
        $usersCourtsBooked = BookingCourt::with('user')->orderBy('updated_at', 'desc')->get()->toArray();


//        foreach($usersCourtsBooked as $key=>$value){
//            $court_schedules=Time_schedule_court::with('court','time_schedule')->where('id',$value['booked_id'])->get()->toArray();
//            if($court_schedules){
//                foreach($court_schedules as $court_schedule){
//                    $usersCourtsBooked[$key]['date']=$court_schedule['date'];
//                    $usersCourtsBooked[$key]['price']=$court_schedule['time_schedule']['price'];
//                    $usersCourtsBooked[$key]['court_name']=$court_schedule['court']['court_name'];
//                    $usersCourtsBooked[$key]['start_hour']=date('g:i A', strtotime($court_schedule['time_schedule']['start_hour']));
//                    $usersCourtsBooked[$key]['end_hour']=date('g:i A', strtotime($court_schedule['time_schedule']['end_hour']));
//                }
//
//
//            }
//
//        }
//        echo "<pre>";
//        print_r($usersCourtsBooked);
//        echo "</pre>";
//        die();
        $this->data['usersCourtsBooked'] = $usersCourtsBooked;
        return view('admin.users.usersCourtsReservationList', $this->data);
    }
//    public function deleteUserCourtBooked($id,$booked_id){
//
//
//        $result=BookingCourt::find($id)->delete();
//        if($result){
//            Time_schedule_court::findOrFail($booked_id)->update(['status'=>'available']);
//            Session::flash('success','User Court Booked is Deleted');
//        }else{
//            Session::flash('error',' Cannot delete User Court Booked');
//        }
//        return redirect()->back();
//    }
    public function usersChangeStatus($id, $status)
    {
        $booking_court = BookingCourt::find($id);
        $user = User::find($booking_court->user_id);
        if ($status == 'Confirmed') {
            Mail::to($user)->send(new UserBookingMail($user, $booking_court->booked_id));

            if (Mail::failures()) {
                return ['error' => 'Cannot Confirmed Booked ,Mail has not been Sent'];
            } else {
                $users_booked_id = explode(',', $booking_court['booked_id']);
                for ($i = 0; $i < count($users_booked_id); $i++) {
                    Time_schedule_court::find($users_booked_id[$i])->update(['status' => 'booked']);
                }


            }
        } elseif ($status == 'Not_Confirmed') {

            Mail::to($user)->send(new UserUnbookingMail($user, $booking_court->booked_id));

            if (Mail::failures()) {
                return ['error' => 'Cannot Confirmed Booked ,Mail has not been Sent'];
            } else {
                $users_booked_id = explode(',', $booking_court['booked_id']);
                for ($i = 0; $i < count($users_booked_id); $i++) {
                    Time_schedule_court::find($users_booked_id[$i])->update(['status' => 'available']);
                }

            }


        } else {
            $users_booked_id = explode(',', $booking_court['booked_id']);

            for ($i = 0; $i < count($users_booked_id); $i++) {
                Time_schedule_court::find($users_booked_id[$i])->update(['status' => 'reservedBooked']);
            }
        }
        BookingCourt::find($id)->update(['status' => $status]);
        return ['status' => $status, 'success' => 'Booked Has been ' . $status . ',Mail has been Sent '];
    }

    public function showUserCourtBooked($id)
    {
        $bookedCourts = BookingCourt::with('user')->find($id)->toArray();

        $total_price = 0;

        $bookedCourts['courtTimeSchedules'] = [];
        $users_booked_id = explode(',', $bookedCourts['booked_id']);
        for ($i = 0; $i < count($users_booked_id); $i++) {

            $bookingCourt = Time_schedule_court::with('court', 'time_schedule')->where('id', $users_booked_id[$i])->first()->toArray();
            array_push($bookedCourts['courtTimeSchedules'], $bookingCourt);
            $total_price += $bookingCourt['time_schedule']['price'];

        }

        $bookedCourts['total_price'] = $total_price;
//        echo "<pre>";
//        print_r($bookedCourts);
//        echo "</pre>";
        return $bookedCourts;

    }
}
