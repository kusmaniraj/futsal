<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Time_schedule_court;
use App\UserActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Illuminate\Support\Facades\Session;

class NotificationController extends Controller
{
    private $data;
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
        //end Traits
        $this->data['title'] = 'Users Notification';

    }
    public function index(){
        $this->data['notifications']=$this->userBookingNotifications();
        return view('admin.notification',$this->data);
    }
    public static function  countUserNotifications(){
        $countUserNotifications = UserActivity::with('user')->where('status', 'unread')->count();
        return $countUserNotifications;
    }

    public static function userBookingNotifications()
    {
        $userNotifications = UserActivity::with('user')->get();

        foreach ($userNotifications as $key => $value) {
            $booked_id = explode(',', $value['booked_id']);
            $courtNames = [];
            $total_price = 0;
            $times = [];
            $dates = [];


            for ($i = 0; $i < count($booked_id); $i++) {
                $court_Schedule = Time_schedule_court::with('court', 'time_schedule')->where('id', $booked_id[$i])->first();
                array_push($courtNames, $court_Schedule['court']['court_name']);
                $total_price += $court_Schedule['time_schedule']['price'];
                array_push($times, date('g:i A', strtotime($court_Schedule['time_schedule']['start_hour'])) . '-' . date('g:i A', strtotime($court_Schedule['time_schedule']['end_hour'])));
                array_push($dates, $court_Schedule['date']);


            }

            $userNotifications[$key]['total_price'] = $total_price;
            $userNotifications[$key]['court_names'] = implode(',', $courtNames);
            $userNotifications[$key]['dates'] = implode(',', $dates);
            $userNotifications[$key]['times'] = implode(',', $times);


        }
        return $userNotifications;
    }
    public function delete($id){
        $result=UserActivity::find($id)->delete();
        if($result){
            Session::flash('success','Notification has been Removed');
        }else{
            Session::flash('error','Notification has not been Removed');
        }
        return redirect()->back();
    }
    public function changeStatus($id){
        $result=UserActivity::find($id)->update(['status'=>'read']);
        if($result){
            return ['success'=>'success change status to read'];
        }else{
            return ['error'=>'cannot change status to read'];
        }

    }
}
