<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Time_schedule_court;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\SettingController;
use App\Traits\Shared;

class DashboardController extends Controller
{
    use Shared;


    private $data;

    public function __construct()
    {
//      Traits
//        setting
        $this->data['getSetting'] = $this->getSetting();
//     Booking   notifications

        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
//        end Traits

        $this->data['title'] = 'Dashboard';
    }

    public function index()
    {



        $this->data['countUsers'] = User::all()->count();
        $this->data['courtBookedCount'] = Time_schedule_court::where(['status' => 'available', 'date' => date("Y-m-d")])->count();
        $this->data['courtUnbookedCount'] = Time_schedule_court::where(['status' => 'booked', 'date' => date("Y-m-d")])->count();

        return view('admin.dashboard', $this->data);
    }
    public function lineChart(){
        $reserveMonth=[];
        $year=date('Y');

        for($i=01;$i<=12;$i++){
            $countBooked= Time_schedule_court::where(['status' => 'booked'])->whereYear('date',$year)->whereMonth('date',$i)->count();
            array_push($reserveMonth,$countBooked);
        }


        return $reserveMonth;
    }

}
