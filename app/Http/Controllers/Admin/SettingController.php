<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Setting;
use File;
use Image;
use App\Traits\Shared;

class SettingController extends Controller
{
    use Shared;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $data;
    public function __construct()
    {
        $this->data['getSetting']=$this->getSetting();
        $this->data['title']='Setting';



    }

    public function index()
    {
        $notifications=new NotificationController();
        $this->data['bookingNotification']=$notifications->userBookingNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();

        return view('admin.setting',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $formInput=$request->all();
        $img=new UploadController();
        $formInput['logo']=$img->imgUpload($formInput['logo']);


        if(Setting::create($formInput)){
            Session::flash('success','ADD Data Successfully');



        }else{
            Session::flash('error',' Cannot Add Data Successfully');
        }
        return redirect('admin/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $formInput=$request->all();
        $image_name=$formInput['oldLogo'];
        if(empty($formInput['logo'])){

            $formInput['logo']=$formInput['oldLogo'];
        }else{
            $img=new UploadController();

            $formInput['logo']=$img->imgUpload($formInput['logo']);
            if($image_name){
                $pathImg=public_path('images/'.$image_name);
                if(file_exists($pathImg) ){
                    unlink($pathImg);
                }
            }
        }

        if(Setting::find($id)->update($formInput)){

            Session::flash('success','Update Data Successfully');



        }else{
            Session::flash('error',' Cannot Update Data Successfully');
        }
        return redirect('admin/setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSetting(){
        return Setting::first();
    }
}
