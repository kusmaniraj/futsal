<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class UploadController extends Controller
{
    public function imgUpload($image){
        if($image){
            $path=public_path('images/');
            if (!file_exists($path)) {
                $result = File::makeDirectory($path, 0775, true);
            }
            $c_img = explode(',', $image)[1];
            $image_data = base64_decode($c_img);
            $image_name= time().'.png';

            Image::make($image_data)->save($path.$image_name);
            return $image_name;


        }
    }
}
