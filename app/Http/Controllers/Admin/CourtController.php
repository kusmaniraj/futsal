<?php

namespace App\Http\Controllers\Admin;

use App\Court;
use App\Time_schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Traits\Shared;

class CourtController extends Controller
{
    private $data;
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
        //end Traits
        $this->data['title'] = 'Court';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $courts=Court::with('time_schedules')->get();
        $this->data['courts']=$courts;
        return view('admin.court.list', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['courts']=Court::orderBy('court_name','asc')->get();
        return view('admin.court.form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'court_name' => 'required|max:20|unique:courts,court_name',

          ]);
        $formInput = $request->all();

        $court = Court::create($formInput);
        $insertCourt['id']=$court->id;
        $insertCourt['court_name']=$formInput['court_name'];

        $insertCourt['success']='Court Added';
        return $insertCourt ;






    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $court=Court::find($id);
        return $court;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'court_name' => 'required|max:20|unique:courts,court_name,'.$id,

        ]);
        $formInput = $request->all();

        $result = Court::find($id)->update($formInput);
        $court= Court::find($id);

        if($result){
            $court['success']='Update Court';
            return $court ;
        }else{
            return ['error'=>'Cannot Update Court'];
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $countCourtPivot=Court::find($id)->time_schedules()->count();
        if($countCourtPivot > 0){
            Court::find($id)->time_schedules()->detach();
        }

        $result=Court::find($id)->delete();


        if($result){
            return ['success'=>'Court is Deleted'];
        }else{
            return ['error'=>'Cannot   Delete Court'];
        }
    }
}
