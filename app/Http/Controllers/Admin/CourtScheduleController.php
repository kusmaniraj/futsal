<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Court;
use App\CourtSchedule;
use App\CourtSchedule_court;
use App\Time_schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use DB;
use App\Traits\Shared;

class CourtScheduleController extends Controller
{
    use Shared;
    private $data;
    public $rule=[
        'date' => 'required|unique:court_schedules',
        'court' => 'required',
        'start_hour' => 'required',
        'end_hour' => 'required|greater_than_field:start_hour',
    ];
    public $message=['end_hour.greater_than_field' => 'End Hour must be Greater than Start hour'];

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['bookingNotification'] = $this->getUserNotifications();
        $this->data['countUserNotifications'] = $this->countNotification();
        //end Traits
        $this->data['title'] = 'Court Schedule';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['court_schedules'] = CourtSchedule::all();

        return view('admin.courtSchedule.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->data['unSelectCourt'] = Court::orderBy('court_name', 'asc')->get();
        return view('admin.courtSchedule.form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rule,$this->message);
        $result = false;

        $formData = $request->all();
        $start_hour = $formData['start_hour'] . ':00:00';
        $end_hour = $formData['end_hour'] . ':00:00';

        $court_schedule = CourtSchedule::create(['date' => $formData['date'], 'start_hour' => $start_hour, 'end_hour' => $end_hour]);

        $insertedTime = $this->insertTime($court_schedule->id, $formData['start_hour'], $formData['end_hour']);

        if ($insertedTime) {
           $result=$this->attachTimeScheduleCourt($formData,$court_schedule->id);
        }


        if ($result) {
            Session::flash('success', 'Successfully Create Court Schedule of Courts');
            return redirect('admin/courtSchedule/');
        } else {
            Session::flash('error', 'Cannot Create Court Schedule of Courts');
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['courts'] = Court::orderBy('court_name', 'ASC')->get();
        $this->data['courtSchedule'] = CourtSchedule::with('courts')->find($id);
        $this->data['courtTimesAndPrices'] = Time_schedule::where('court_schedule_id',$id)->get()->toArray();
        $court_schedule_courts=CourtSchedule_court::where('court_schedule_id',$id)->get();
       $unSelectCourt=[];
        foreach($court_schedule_courts as $court_schedule_court){

            array_push($unSelectCourt,$court_schedule_court['court_id']);
        }
       $this->data['unSelectCourt']=Court::whereNotIn('id',$unSelectCourt)->get()->toArray();




        return view('admin.courtSchedule.form', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required|unique:court_schedules,date,' . $id,
            'court' => 'required',
            'start_hour' => 'required',
            'end_hour' => 'required|greater_than_field:start_hour',
        ], ['end_hour.greater_than_field' => 'End Hour must be Greater than Start hour']);
        $result = false;

        $formData = $request->all();
        $start_hour = $formData['start_hour'] . ':00:00';
        $end_hour = $formData['end_hour'] . ':00:00';
        $checkUserReserveCourts=$this->checkUserReserveCourts();
        if($checkUserReserveCourts==false){
            Session::flash('error', 'Cannot UPDATE Court, User Has Reserved Courts !');
            return redirect()->back();
        }

        $court_schedule = CourtSchedule::find($id)->update(['date' => $formData['date'], 'start_hour' => $start_hour, 'end_hour' => $end_hour]);

        $insertedTime = $this->insertTime($id, $formData['start_hour'], $formData['end_hour']);

        if ($insertedTime) {
            CourtSchedule::find($id)->courts()->detach();
            $result=$this->attachTimeScheduleCourt($formData,$id);
        }


        if ($result) {
            Session::flash('success', 'Successfully Update Court Schedule of Courts');
            return redirect('admin/courtSchedule/');
        } else {
            Session::flash('error', 'Cannot U[date Court Schedule of Courts');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkUserReserveCourts=$this->checkUserReserveCourts();
        if($checkUserReserveCourts==false){
            Session::flash('error', 'Cannot DELETE Court, User Has Reserved Courts !');
            return redirect()->back();
        }

        $countTimeSchedule = Time_schedule::where('court_schedule_id', $id)->count();
        if ($countTimeSchedule > 0) {
            Time_schedule::where('court_schedule_id', $id)->delete();
        }


        $countPivot = CourtSchedule::find($id)->courts()->count();
        if ($countPivot > 0) {
            CourtSchedule::find($id)->courts()->detach();
        }


        $result = CourtSchedule::find($id)->delete();


        if ($result) {
            Session::flash('success', 'Successfully ! Delete Court Time Schedule ');
        } else {
            Session::flash('error', 'Cannot ! Delete Court Time Schedule ');
        }
        return redirect()->back();
    }

    public function insertTime($court_schedule_id, $start_hour, $end_hour)
    {
        $result = false;

        if ($court_schedule_id) {
            DB::table('time_schedules')->where('court_schedule_id', $court_schedule_id)->delete();
        }


        for ($i = $start_hour; $i < $end_hour; $i++) {


            Time_schedule::create(['court_schedule_id' => $court_schedule_id, 'start_hour' => $i . ":00", 'end_hour' => ($i + 1) . ':00']);
            $result = true;

        }
        return $result;
    }
    public function attachTimeScheduleCourt($formData,$court_schedule_id){
        $time_schedules = Time_schedule::where(['court_schedule_id' => $court_schedule_id])->get();

        for ($i = 0; $i < count($formData['court']); $i++) {
            CourtSchedule::find($court_schedule_id)->courts()->attach($formData['court'][$i]);
            foreach ($time_schedules as $time_schedule) {


                Court::find($formData['court'][$i])->time_schedules()->attach($time_schedule->id, ['date' => $formData['date']]);


            }




        }


//            insert price
        $this->insertPrices($formData['price'],$court_schedule_id);
        return true;

    }
    public function insertPrices($prices,$court_schedule_id){
        $time_schedules = Time_schedule::where(['court_schedule_id' => $court_schedule_id])->get();
        for ($i = 0; $i < count($prices); $i++) {
            foreach ($time_schedules as $key=> $time_schedule) {
                if($key==$i){
                    Time_schedule::find($time_schedule->id)->update(['price'=>$prices[$i]]);

                }
            }

        }
        return true;
    }

    public function setTime(Request $request)
    {
        $this->validate($request, [

            'start_hour' => 'required',
            'end_hour' => 'required|greater_than_field:start_hour',
        ], ['end_hour.greater_than_field' => 'End Hour must be Greater than Start hour']);
        $formData = $request->all();

        $this->data['courtTimes'] = [];
        for ($i = $formData['start_hour']; $i < $formData['end_hour']; $i++) {
            $time = ['start_hour' => $i . ":00", 'end_hour' => ($i + 1) . ':00'];
            array_push($this->data['courtTimes'], $time);


        }

        return view('admin.courtSchedule/ajaxCourtTimeAndPriceForm', $this->data);
    }
    public function checkUserReserveCourts(){
        $date=date('Y-m-d');
        $bookingCourtLists = BookingCourt::where('updated_at','>=', $date)->count();
        if($bookingCourtLists > 0){
           return false;
        }else{
            return true;
        }
    }
}
