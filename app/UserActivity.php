<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table='user_activities';
    protected $fillable=['id','user_id','booked_id','status','type'];
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
