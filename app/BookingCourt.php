<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCourt extends Model
{
protected $table='booking_courts';
    protected $fillable=['id','booked_id','user_id','status','note'];
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
