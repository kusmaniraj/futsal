<?php

namespace App\Mail;

use App\Http\Controllers\Admin\SettingController;
use App\Time_schedule_court;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Traits\Shared;
class UserBookingMail extends Mailable
{
    use Queueable, SerializesModels,Shared;
    protected $data;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$booked_id)
    {
        $this->data['user'] = $user;
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits


        $this->data['bookingCourtList']=$this->getBookingCourtList($booked_id);

//       echo "<pre>";
//        print_r( $this->data['bookingCourtList']);
//        echo "</pre>";
//        die();





    }
    public function getBookingCourtList($booked_id){
        $users_booked_id=explode(',',$booked_id);
        $courtNames=[];
        $total_price=0;
        $times=[];
        $dates=[];
        $prices=[];

        for($i=0;$i<count($users_booked_id);$i++) {
            $bookingCourt= Time_schedule_court::with('court', 'time_schedule')->where('id', $users_booked_id[$i])->first();
           $total_price+=$bookingCourt['time_schedule']['price'];
            array_push($courtNames,$bookingCourt['court']['court_name']);
            array_push($prices,$bookingCourt['time_schedule']['price']);


            array_push($times,date('g:i A', strtotime($bookingCourt['time_schedule']['start_hour'])).'-'.date('g:i A', strtotime($bookingCourt['time_schedule']['end_hour'])));
            array_push($dates,$bookingCourt['date']);
        }
        $bookingCourtList['court_names']=$courtNames;
        $bookingCourtList['total_price']=$total_price;
        $bookingCourtList['times']=$times;
        $bookingCourtList['dates']=$dates;
        $bookingCourtList['updated_at']=$bookingCourt['updated_at'];
        $bookingCourtList['prices'] =$prices;


        return $bookingCourtList;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Confirmed Booking Courts')
                    ->view('admin.userBookingMail',$this->data);
    }
}
