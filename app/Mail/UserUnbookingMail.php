<?php

namespace App\Mail;

use App\Http\Controllers\Admin\SettingController;
use App\Time_schedule_court;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\User;
use App\Traits\Shared;

class UserUnbookingMail extends Mailable
{
    use Queueable, SerializesModels;
    use Shared;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $data;
    public function __construct(User $user,$booked_id)
    {
        $this->data['user'] = $user;
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits


        $this->data['unBookingCourtList']=$this->getUnBookingCourtList($booked_id);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('UnConfirmed Booking Courts')->view('admin.userUnBookingMail',$this->data);
    }
    public function getUnBookingCourtList($booked_id){
        $users_booked_id=explode(',',$booked_id);
        $courtNames=[];
        $total_price=0;
        $times=[];
        $dates=[];
        $prices=[];

        for($i=0;$i<count($users_booked_id);$i++) {
            $bookingCourt= Time_schedule_court::with('court', 'time_schedule')->where('id', $users_booked_id[$i])->first();
            $total_price+=$bookingCourt['time_schedule']['price'];
            array_push($courtNames,$bookingCourt['court']['court_name']);
            array_push($prices,$bookingCourt['time_schedule']['price']);


            array_push($times,date('g:i A', strtotime($bookingCourt['time_schedule']['start_hour'])).'-'.date('g:i A', strtotime($bookingCourt['time_schedule']['end_hour'])));
            array_push($dates,$bookingCourt['date']);
        }
        $unBookingCourtList['court_names']=$courtNames;
        $unBookingCourtList['total_price']=$total_price;
        $unBookingCourtList['times']=$times;
        $unBookingCourtList['dates']=$dates;
        $unBookingCourtList['updated_at']=$bookingCourt['updated_at'];
        $unBookingCourtList['prices'] =$prices;


        return $unBookingCourtList;

    }
}
