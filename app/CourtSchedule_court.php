<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourtSchedule_court extends Model
{
    protected $table='court_schedule_courts';
    protected $fillable=['court_id','court_schedule_id'];
}
