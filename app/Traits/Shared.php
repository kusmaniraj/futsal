<?php namespace App\Traits;

use App\Http\Controllers\Admin\NotificationController;
use App\Setting;
use App\Time_schedule_court;
use App\UserActivity;

trait Shared
{
    public function getSetting()
    {
        return Setting::first();
    }
    public function getUserNotifications(){


        return NotificationController::userBookingNotifications();
    }

    public function countNotification(){
        return NotificationController::countUserNotifications();
    }
}
