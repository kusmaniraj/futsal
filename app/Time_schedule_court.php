<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time_schedule_court extends Model
{
    protected $table='time_schedules_court';
    protected $fillable=['court_id','time_schedule_id','status','id','date'];
    public function court(){
        return$this->hasOne('App\Court','id','court_id');
    }
    public function time_schedule(){
        return$this->hasOne('App\Time_schedule','id','time_schedule_id');
    }
}
