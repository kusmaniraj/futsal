<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourtSchedule extends Model
{
    protected $table='court_schedules';
    protected $fillable=['date','start_hour','end_hour'];
    public function courts(){
       return $this->belongsToMany('App\Court','court_schedule_courts','court_schedule_id','court_id')->withTimestamps();
    }
}
