<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
    protected $table='courts';
    protected $fillable=['court_name'];
    public function time_schedules(){
       return $this->belongsToMany('App\Time_schedule','time_schedules_court','court_id','time_schedule_id')->withTimestamps()->withPivot('status','date');
    }
}
