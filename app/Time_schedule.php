<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time_schedule extends Model
{
    protected $table='time_schedules';
    protected $fillable=['start_hour','end_hour','court_schedule_id','price'];
    public function time_schedule_courts(){
        return $this->hasMany('App\Time_schedule_court','time_schedule_id');
    }
}

