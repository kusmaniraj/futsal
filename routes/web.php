<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'namespace' => 'Auth'], function () {
    Route::middleware(['auth:admin'])->group(function () {
        Route::get('/', 'AdminController@index')->name('admin.dashboard');
    });
    Route::middleware(['guest:admin'])->group(function () {
        Route::get('/login', 'AdminController@showLoginForm')->name('admin.login');
        Route::post('/login', 'AdminController@login')->name('admin.login.submit');
        Route::get('/register', 'AdminController@showRegisterForm')->name('admin.register');
        Route::post('/register', 'AdminController@register')->name('admin.register.submit');

        Route::get('password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');;
        Route::get('password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('admin.password.reset');;
        Route::post('password/reset', 'AdminResetPasswordController@reset');


    });
    Route::get('/logout', 'AdminController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth:admin']], function () {
    Route::resource('/setting', 'SettingController');
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::prefix('/dashboard')->group(function(){
        Route::get('/lineChart', 'DashboardController@lineChart');


    });
    Route::prefix('/notification')->group(function(){
        Route::get('/', 'NotificationController@index')->name('admin.notificationList');
        Route::get('/delete/{id}', 'NotificationController@delete')->name('admin.notification.delete');
        Route::get('/changeStatus/{id}', 'NotificationController@changeStatus')->name('admin.notification.changeStatus');

    });



    Route::get('/profile', 'ProfileController@index')->name('admin.profile');
    Route::post('/profile', 'ProfileController@update')->name('admin.profile.update');

    Route::resource('/court', 'CourtController');

    Route::resource('/time_schedule', 'TimeScheduleController');
    Route::resource('/court_booking', 'CourtBookingController');
    Route::resource('/courtSchedule', 'CourtScheduleController');
    Route::post('/courtSchedule/setTime', 'CourtScheduleController@setTime')->name('admin.setTime');
    Route::get('/usersList', 'UserController@usersList')->name('admin.usersList');
    Route::get('/showUserCourtBooked/{id}', 'UserController@showUserCourtBooked')->name('admin.showUserCourtBooked');
    Route::get('/usersChangeStatus/{id}/{status}', 'UserController@usersChangeStatus')->name('admin.usersChangeStatus');
    Route::get('/usersCourtsReservation', 'UserController@usersCourtsReservation')->name('admin.usersCourtsReservation');
    Route::get('/userDelete/{id}', 'UserController@delete')->name('admin.userDelete');
    Route::get('/deleteUserCourtBooked/{id}/{booked_id}', 'UserController@deleteUserCourtBooked')->name('admin.deleteUserCourtBooked');

});


Route::group(['namespace' => 'Web'], function () {
    Route::post('/bookingCourt', 'BookingController@bookingCourt')->name('web.bookingCourt');


    Route::get('/', 'HomeController@index')->name('web.home');
    Route::get('/viewCourt', 'HomeController@viewCourt')->name('web.viewCourt');
    Route::post('/ajax_selected_court/{id}', 'HomeController@ajax_selected_court');
    Route::get('/ajaxSelectCourtSchedule/{date}', 'HomeController@ajaxSelectCourtSchedule');

    Route::middleware(['auth:web'])->group(function () {

        Route::get('/accountSetting', 'AccountSettingController@showAccountSettingForm')->name('web.accountSetting');
        Route::post('/accountSetting/update', 'AccountSettingController@updateAccountSetting')->name('web.updateAccountSetting');

        Route::get('/bookingCourtList', 'BookingController@bookingCourtList')->name('web.bookingCourtList');
        Route::get('/userReservationTypeList/{type}', 'BookingController@userReservationTypeList')->name('web.userReservationTypeList');

        Route::get('/showUserCourtBooked/{id}', 'BookingController@showUserCourtBooked')->name('web.showUserCourtBooked');
//        Route::get('/printUserCourtBooked','BookingController@printUserCourtBooked')->name('web.printUserCourtBooked');

        Route::get('/removeCourt/{id}', 'BookingController@removeCourt')->name('web.removeCourt');


    });
    Route::middleware(['guest:web'])->group(function () {
        Route::post('/ajaxLogin', 'HomeController@ajaxLogin')->name('web.ajaxLogin');
    });


});
