<!DOCTYPE html>
<html lang="en">
@include('admin.include.head')


<body class="nav-md">
<!--<div id="loading" style="display: none">Loading..... <img src="{{asset('shared/ajax-loader.gif')}}" alt=""></div>-->

<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            @include('admin.include.sidebar')
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            @include('admin.include.nav_menu')

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            @include('admin.include.footer')

        </footer>
        <!-- /footer content -->
    </div>
</div>


<!-- FastClick -->
<script src="{{asset('adminTemplate/asset/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('adminTemplate/asset/nprogress/nprogress.js')}}"></script>
<script src="{{asset('adminTemplate/asset/datatables/datatables.js')}}"></script>
<!--DatePicker-->
<script src="{{asset('adminTemplate/asset/datePicker/bootstrap-datepicker.js')}}"></script>


<!-- Custom Theme Scripts -->
<script src="{{asset('adminTemplate/build/js/custom.js')}}"></script>

<script>
    var base_url = window.location.origin;
</script>
<script src="{{asset('shared/ajaxHelper.js')}}"></script>
<script>
    $("#msg-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });
</script>
<script>
    $('.remove').click(function () {
        var ask_permission = confirm("Are you sure,you want to Delete ?");
        if (ask_permission == true) {
            return true;
        }
        else {
            return false;
        }
    });
</script>
<script src="{{asset('shared/loading.js')}}"></script>
<script src="{{asset('pages/admin/notification.js')}}"></script>
@stack('scripts')
</body>
</html>
