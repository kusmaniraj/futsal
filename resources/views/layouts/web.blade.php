<!DOCTYPE html>
<html lang="en-AU">

<!-- Mirrored from pittwater-rsl-futsal.yepbooking.com.au/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 Jan 2018 05:42:46 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
@include('web.include.head')
<body style="background-image:url({{asset('futsal/images/body.jpg')}});">
<div class="container">
   @include('web.include.header')
    @yield('content')
    @include('web.include.footer')

</div>


//<!--    datePicker-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>-->






</body>
<!--<script>-->
<!--    $("#msg-alert").fadeTo(2000, 500).slideUp(500, function(){-->
<!--        $("#success-alert").slideUp(500);-->
<!--    });-->
<!--</script>-->
<script>
    var base_url = window.location.origin;
</script>




<script src="{{asset('futsal/datatables/datatables.js')}}"></script>
<script src="{{asset('futsal/datePicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('shared/ajaxHelper.js')}}"></script>



@stack('scripts')


</html>
