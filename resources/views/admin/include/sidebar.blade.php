<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>{{$getSetting ? $getSetting['website_name'] : " Template"}}</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        <div class="profile_pic">
            <img src="{{asset('images/ss.jpg')}}" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>

            <h2>{{Auth::user()->name}}</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /menu profile quick info -->

    <br/>

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-home"></i> Dashboard </span></a>


                <li><a><i class="fa fa-futbol-o"></i> Courts Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{route('court.index')}}"><i class="fa fa-home"></i> Courts </span></a>
                        <li><a href="{{route('courtSchedule.index')}}"><i class="fa fa-calendar"></i> Court Schedule </span></a>
<!--                        <li><a href="{{route('court_booking.index')}}"><i class="fa fa-home"></i> Courts Booking Status </span></a>-->
                    </ul>
                </li>

                <li><a><i class="fa fa-users"></i> Users Management  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{route('admin.usersList')}}"><i class="fa fa-users"></i>Users</span></a>
                        <li><a href="{{route('admin.usersCourtsReservation')}}"><i class="fa fa-users"></i>Users Courts Reservation</span></a>

                    </ul>
                </li>
            </ul>





        </div>


    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a href="{{url('admin/setting/')}}" data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('admin.logout')}}">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
</div>