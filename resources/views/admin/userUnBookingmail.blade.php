<html>


<head>
    <title>Not Confirmed Booking Courts</title>

</head>
<body>
<h2>Hello {{$user->name}} Welcome to {{$getSetting['website_name']}}</h2>

<div>
    <h5>Date:&nbsp;{{date('d F Y D', strtotime($unBookingCourtList['updated_at']))}}</h5>
</div>
<table style="border: none">
    <tbody>
    <tr>


        <th>Court Name</th>
        <th>Court Book Date</th>
        <th>Booking Time</th>
        <th>Price</th>

    </tr>

    @isset($unBookingCourtList)
    @for($i=0; $i < count($unBookingCourtList['court_names']);$i++)
    <tr>

        <td>{{$unBookingCourtList['court_names'][$i]}}</td>
        <td>{{$unBookingCourtList['dates'][$i]}}</td>
        <td>{{$unBookingCourtList['times'][$i]}}</td>
        <td>{{$unBookingCourtList['prices'][$i]}}</td>

    </tr>

    @endfor
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <th>Total Price :</th>
        <th>{{$unBookingCourtList['total_price']}}</th>
    </tr>


    @endisset


    </tbody>

</table>
<div class="row">
    <h3 class="text-danger">Sorry !</h3>
    <p>Your Reserved Courts has been UnConfirmed</p>
</div>
<div style="text-align: center">
    Copyright of @ 2018 <a href="{{url('/')}}">{{$getSetting ? $getSetting['website_name'] : " Template"}}</a>
</div>
<div class="clearfix"></div>

</body>
</html>
