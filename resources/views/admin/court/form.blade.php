@extends('layouts.admin')

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        @include('alertMessages')
                        <form action="{{route('court.store')}}" method="post" enctype=multipatr/form-data>
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('court_name') ? ' has-error' : '' }}">
                                <label for="court_name">Court Name</label>
                                <input type="text" class="form-control" id="court_name" name="court_name"
                                       value="{{old('court_name')}}">
                                @if ($errors->has('court_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('court_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('court_price') ? ' has-error' : '' }}">
                                <label for="court_price">Court Price</label>
                                <input type="text" class="form-control" id="court_price" name="court_price"
                                       value="{{old('court_price')}}">
                                @if ($errors->has('court_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('court_price') }}</strong>
                                    </span>
                                @endif
                            </div>



                            <div class="form-group">
                                <button class="btn btn-primary mr15" type="submit">Save Changes</button>
                                <button class="btn btn-danger" type="button" id="cancelBtn"
                                        onclick="window.history.back()">Cancel
                                </button>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
