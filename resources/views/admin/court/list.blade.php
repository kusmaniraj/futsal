@extends('layouts.admin')

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            @include('alertMessages')
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="pull-right">
                            <a class="btn btn-primary" id="addCourtsBtn">Add Court</a>
                            
                        </div>


                        <table id="courtTable" class="table table-bordered">
                            <thead>
                            <tr >
                                <th>SN</th>
                                <th>Court Name</th>


                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($courts)
                            @foreach($courts as $key => $court)

                            <tr id="court{{$court['id']}}"  >
                                <td id="key{{$court['id']}}" data-id="{{$key+1}}">{{$key+1}}</td>
                                <td>{{$court['court_name']}}</td>


                                <td>
                                    <a data-id="{{$court['id']}}"  class="btn btn-primary editCourtBtn"><i class="fa fa-edit"></i></a>
                                    <a data-id="{{$court['id']}}"  class="btn btn-danger  delCourtBtn"  id=""><i class="fa fa-remove"></i></a>
                                </td>



                            @endforeach
                            @endisset                          </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.court.courtFormModal')
@endsection
@push('scripts')

<script src="{{asset('pages/admin/court.js')}}"></script>

@endpush



