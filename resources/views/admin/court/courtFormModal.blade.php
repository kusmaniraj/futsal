<div id="courtModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Fustal Court</h4>
            </div>
            <div class="modal-body">


                <form enctype=multipatr/form-data id="courtForm" method="post">
                    <input type="hidden" name="id">
                    {{ csrf_field() }}

                    <div class="form-group" id="court_name">
                        <label for="court_name">Court Name</label>
                        <input type="text" class="form-control" name="court_name"
                               value="{{old('court_name')}}">

                        <div class="error_msg hidden"></div>

                    </div>


                    <div class="form-group">
                        <button class="btn btn-primary " type="submit">Create Court</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>