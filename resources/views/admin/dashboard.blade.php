@extends('layouts.admin')
<style>
    .reservation{
        background: #3fbaa2;
        width: 30px;
        display: block;

        color: #3fbaa2;
    }
    .visit{
        background: #3e7f8d;
        width: 30px;
        display: block;

        color: #3e7f8d;
    }
</style>

@section('content')
<div role="main" style="min-height: 1704px;">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-users"></i></div>
                <div class="count">{{$countUsers}}</div>
                <h3>Sign Up</h3>

            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-comments-o"></i></div>
                <div class="count">179</div>
                <h3>User Messages</h3>

            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-futbol-o"></i></div>
                <div class="count"><span class="text-success">{{$courtUnbookedCount}}  </span><span class="text-danger">{{$courtBookedCount}} </span>
                </div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-4 ">
                        <i class="fa fa-square green"></i><span style="margin-left: 10px; color: #000">Booked</span>
                    </div>
                    <div class="col-md-4 ">
                        <i class="fa fa-square red"></i><span style="margin-left: 10px; color: #000">Available</span>
                    </div>

                </div>
                <h3>Futsal Court &nbsp {{date('Y-m-d')}}</h3>


            </div>
        </div>

    </div>

    <!--line chart-->
    <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>User

                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-4 ">
                            <i class="fa fa-square reservation "></i><span style="margin-left: 10px; color: #000">Court Reserve</span>
                        </div>
                        <div class="col-md-4 ">
                            <i class="fa fa-square visit"></i><span style="margin-left: 10px; color: #000">Visit</span>
                        </div>

                    </div>
                    <canvas id="lineChart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{asset('adminTemplate/asset/chart/chart.js')}}"></script>
<script src="{{asset('pages/admin/lineChart.js')}}"></script>
@endpush
