@extends('layouts.admin')

@push('styles')
<style>
    #notificationTable tbody tr td{
        cursor: pointer;
    }
    #notificationTable tbody tr td:hover{
        background: #2a3f54;
        color: #fff;
    }
</style>
@endpush
@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">

            <div class="x_panel">
                <div class="x_content">
                    <div class="row">

                        @include('alertMessages')

                        <table id="notificationTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>

                                <th>Particulars</th>
                                <th>Status</th>
                                <th>Action</th>





                            </tr>
                            </thead>
                            <tbody>
                            @isset($notifications)
                            @foreach($notifications as $key => $notification)

                            <tr>
                                <td>{{$key+1}}</td>

                                <td class="notificationStatus"  data-id="{{$notification['id']}}">{{$notification->updated_at->diffForHumans()}},&nbsp;{{$notification['user']['name']}}  &nbsp; ({{$notification['user']['email']}} )&nbsp; has reserve {{$notification['court_names']}} at {{$notification['times']}} on {{$notification['dates']}}</td>



                                <td><span class="label {{($notification['status']=='read') ? 'label-success':'label-danger'}}">{{$notification['status']}}</span></td>
                                <td>

                                    <a href="{{route('admin.notification.delete',$notification['id'])}}"  class="btn btn-danger btn-sm remove  "  id=""><i class="fa fa-remove"></i></a>
                                </td>

                                @endforeach
                                @endisset
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection




