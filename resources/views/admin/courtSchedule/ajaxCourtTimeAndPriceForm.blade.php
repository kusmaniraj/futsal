
    @isset($courtTimes)
    @foreach($courtTimes as $time)
    <div class="form-group col-md-6">

        <div class="col-md-6">
            <label for="time"> {{date("g:i A",
                strtotime($time['start_hour'])).'-'.date("g:i A",
                strtotime($time['end_hour']))}}</label>

        </div>
        <div class="col-md-3">

            <input type="number" name="price[]" class="form-control" id="price" placeholder="Add Price" required>
        </div>
    </div>
    @endforeach
    @endisset
