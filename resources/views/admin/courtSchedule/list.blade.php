@extends('layouts.admin')

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            @include('alertMessages')
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="pull-right">
                            <a href="{{route('courtSchedule.create')}} " class="btn btn-primary">Add Schedule For Court</a>
                            
                        </div>


                        <table id="courtTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Date</th>

                                <th>Time Schedule</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($court_schedules)
                            @foreach($court_schedules as $key => $court_schedule)

                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$court_schedule['date']}}</td>

                                <td>{{date("g:i A", strtotime($court_schedule['start_hour']))}}-{{date("g:i A", strtotime($court_schedule['end_hour']))}}</td>
                                <td>



                                    <form class="col-md-2" action="{{route('courtSchedule.destroy',$court_schedule['id'])}}"  method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit"  class="btn btn-danger remove" ><i class="fa fa-remove"></i></button>
                                    </form>
                                    <a href="{{route('courtSchedule.edit',$court_schedule['id'])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>

                                </td>



                            @endforeach
                            @endisset                          </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script>
    $('#courtTable').DataTable({
        processing: true,


    });
</script>


@endpush



