@isset($courtSchedule)
@php
$date=$courtSchedule['date'];
$start_hour=$courtSchedule['start_hour'];
$end_hour=$courtSchedule['end_hour'];
$state='Update';
$id=$courtSchedule['id'];
$route=route('courtSchedule.update',$id);
$method=method_field('PUT');

@endphp
@else
@php
$date="";

$state="Add";
$route=route('courtSchedule.store');
$method='';


@endphp
@endisset
@extends('layouts.admin')

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        @include('alertMessages')
                        <form id="courtScheduleForm" action="{{$route}}" method="post"
                              enctype=multipatr/form-data>
                            {{ csrf_field() }}
                            {{$method}}

                            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} ">
                                <label for="date">Date</label>

                                <div class="input-group date col-md-2">

                                    <input style="cursor: pointer" type="text" class="form-control has-feedback-left "
                                           name="date"
                                           value="{{$date ? $date :old('date')}}" id="datepicker" readonly>
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                </div>

                                @if ($errors->has('date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('court') ? ' has-error' : '' }}">

                                <label for="Courts">Futsal Courts</label>
                                @if ($errors->has('court'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('court') }}</strong>
                                    </span>
                                @endif
                                <div class="row" id="addedCourts">
                                    <div class="col-md-10">

                                        @isset($courtSchedule['courts'])
                                        @isset($courts)
                                        @foreach($courts as $court)
                                        @foreach($courtSchedule['courts'] as $selectedCourt)
                                        @if($selectedCourt['id']==$court['id'])

                                        <div class="col-md-3">
                                            <input checked type="checkbox" class="form-inline" id="{{$court['id']}}"
                                                   name="court[]" value="{{$court['id']}}">
                                            <label for="{{$court['id']}}">{{$court['court_name']}}</label>
                                        </div>

                                        @endif


                                        @endforeach
                                        @endforeach


                                        @endisset  <!--End of Courts isset-->

                                        <!--End of SelectedCourts isset-->
                                        @endisset
<!--                                        unselect Court-->
                                        @isset($unSelectCourt)
                                        @foreach($unSelectCourt as $court)
                                        <div class="col-md-3">
                                            <input  type="checkbox" class="form-inline" id="{{$court['id']}}"
                                                    name="court[]" value="{{$court['id']}}">
                                            <label for="{{$court['id']}}">{{$court['court_name']}}</label>
                                        </div>
                                        @endforeach
                                        @endisset

                                    </div>
                                    <button class="btn btn-info pull-right" id="addCourtsBtn" type="button">Add Courts
                                    </button>


                                </div>


                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div
                                        class=" col-md-4 form-group {{ $errors->has('start_hour') ? ' has-error' : '' }}">
                                        <label for="start_hour">Start Time</label>


                                        @isset($start_hour)
                                        <input type="hidden" name="start_hour"
                                               value="{{str_replace(':00','',$start_hour)}}">
                                        <select name="start_hour" class="form-control " disabled>
                                            <option value="0" disabled>select start time</option>
                                            <option value="{{str_replace(':00','',$start_hour)}}" readonly selected>
                                                {{date("g:i A",
                                                strtotime($start_hour))}}
                                            </option>
                                        </select>

                                        @else
                                        <input type="hidden" name="start_hour"
                                               value="">
                                        <select name="start_hour" class="form-control ">
                                            <option value="0" disabled selected>select start time</option>
                                            @include('admin.time_schedule')
                                        </select>

                                        @endisset


                                        @if ($errors->has('start_hour'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('start_hour') }}</strong>
                                    </span>
                                        @endif

                                        <div class="error_msg hidden"></div>

                                    </div>


                                    <div
                                        class=" col-md-4 form-group {{ $errors->has('end_hour') ? ' has-error' : '' }}">
                                        <label for="end_hour">End Time</label>


                                        @isset($end_hour)
                                        <input type="hidden" name="end_hour"
                                               value="{{str_replace(':00','',$end_hour)}}">
                                        <select name="end_hour" class="form-control " disabled>
                                            <option value="0" disabled>select end hour</option>
                                            <option value="{{str_replace(':00','',$end_hour)}}" readonly selected>
                                                {{date("g:i A",
                                                strtotime($end_hour))}}
                                                @include('admin.time_schedule')
                                            </option>
                                        </select>

                                        @else
                                        <input type="hidden" name="end_hour"
                                               value="">
                                        <select name="end_hour" class="form-control ">
                                            <option value="0" disabled selected>select end time</option>
                                            @include('admin.time_schedule')
                                        </select>
                                        @endisset


                                        @if ($errors->has('end_hour'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('end_hour') }}</strong>
                                    </span>
                                        @endif


                                        <div class="error_msg hidden"></div>

                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label class="col-md-12" for="set">&nbsp</label>
                                        @isset($end_hour)

                                        <div class="col-md-4">
                                            <button type="button" class=" btn btn-sm btn-primary form-control hidden "
                                                    id="setTimeBtn">Set
                                            </button>
                                        </div>
                                        <div class="col-md-2" id="refreshTimeBtn">
                                            <a href="{{url('admin/courtSchedule/'.$id.'/edit')}}"
                                               class=" btn btn-md btn-info form-control "
                                            ><i class="fa fa-refresh"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class=" btn btn-md btn-danger form-control   "
                                                    id="removeSetTimeBtn">Unset&nbsp;<i class="fa fa-remove"></i>
                                            </button>

                                        </div>

                                        @else
                                        <div class="col-md-4">
                                            <button type="button" class=" btn btn-sm btn-primary form-control "
                                                    id="setTimeBtn">Set
                                            </button>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class=" btn btn-md btn-danger form-control hidden"
                                                    id="removeSetTimeBtn">Unset&nbsp;<i class="fa fa-remove"></i>
                                            </button>

                                        </div>
                                        @endisset


                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div id="setCourtTimeAndPrice">
                                    @isset($courtTimesAndPrices)
                                    @foreach($courtTimesAndPrices as $courtTimesAndPrice)
                                    <div class="form-group col-md-6">

                                        <div class="col-md-6">
                                            <label for="time"> {{date("g:i A",
                                                strtotime($courtTimesAndPrice['start_hour'])).'-'.date("g:i A",
                                                strtotime($courtTimesAndPrice['end_hour']))}}</label>

                                        </div>
                                        <div class="col-md-3">

                                            <input type="number" name="price[]" value="{{$courtTimesAndPrice['price']}}"
                                                   class="form-control" id="price" placeholder="Add Price" required>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endisset
                                </div>

                            </div>


                            <div class="form-group pull-right ">
                                <button class="btn btn-primary " type="submit">{{$state}} Changes</button>

                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.court.courtFormModal')
@endsection
@push('scripts')
<script>
    $('#courtScheduleForm').on('click', '#addCourtsBtn', function () {
        $('#courtModal').modal('show');
        $('#courtModal').find('input[type="text"],input[type="number"]').val('');
        $('label').closest('.form-group').removeClass('has-error');
        $('input').next('.error_msg').html('');


    });

    //setTime
    $('#courtScheduleForm').on('click', '#setTimeBtn', function (e) {
        e.preventDefault();


        $('.error_msg').html('');
        $('label').closest('.form-group').removeClass('has-error');
        var startHour = $('#courtScheduleForm select[name="start_hour"]').val();
        var endHour = $('#courtScheduleForm select[name="end_hour"]').val();
        var formData = {
            start_hour: startHour,
            end_hour: endHour,
            _token: $('meta[name="csrf-token"]').attr('content')
        };
        new ajaxHelper([base_url + "/admin/courtSchedule/setTime", formData, "post", ""]).makeAjaxCall().then(function (data) {
            $('#courtScheduleForm #setCourtTimeAndPrice').html(data);
            $('#removeSetTimeBtn').removeClass('hidden');
            $('#courtScheduleForm select').attr('disabled', true);
            $('#courtScheduleForm input[name="start_hour"]').val(startHour);
            $('#courtScheduleForm input[name="end_hour"]').val(endHour);
            $('#setTimeBtn').addClass('hidden');


        }, function (err) {
            $('.error_msg').removeClass('hidden');
            var data = JSON.parse(err.responseText);
            $.each(data.errors, function (i, value) {
                $('label[for="' + i + '"]').closest('.form-group').addClass('has-error');
                $('select[name="' + i + '"]').next('.error_msg').html('<span class="help-block">' +
                    '<strong>' + value + '</strong>' +
                    '</span>');
            });

        });

    });

    //    removeSetTime
    $('#courtScheduleForm').on('click', '#removeSetTimeBtn', function (e) {
        e.preventDefault();
        if (confirm('Are you Sure want to Unset Time ?')) {
            $('#courtScheduleForm select[name="start_hour"]').val(0);
            $('#courtScheduleForm select[name="end_hour"]').val(0)
            $('#courtScheduleForm select').attr('disabled', false);
            $('#courtScheduleForm input[name="start_hour"]').val('');
            $('#courtScheduleForm input[name="end_hour"]').val('');
            $('#courtScheduleForm #setCourtTimeAndPrice').html('');

            $('#removeSetTimeBtn').addClass('hidden');
            $('#setTimeBtn').removeClass('hidden');


        } else {
            return false;
        }


    });

</script>
<script>
    $('#courtModal #courtForm').on('submit', function (e) {
        e.preventDefault();

        var formData = $(this).serialize();
        new ajaxHelper([base_url + "/admin/court", formData, "post", ""]).makeAjaxCall().then(function (data) {


            $('#addedCourts').append('<div class="col-md-3">' +
                '<input type="checkbox" class="form-inline" id="' + data.id + '" name="court[]" value="' + data.id + '">' +
                '<label for="' + data.id + '">' + data.court_name + '</label>' +
                '</div> ');
            $('#courtModal').modal('hide');

            $('#success-msg-alert').show().find('strong').html('Successfully' + data.success);
            $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');


        }, function (err) {
            $('.error_msg').removeClass('hidden');

            var data = JSON.parse(err.responseText);

            $.each(data.errors, function (i, value) {
                $('label[for="' + i + '"]').closest('.form-group').addClass('has-error');
                $('input[name="' + i + '"]').next('.error_msg').html('<span class="help-block">' +
                    '<strong>' + value + '</strong>' +
                    '</span>');
            })
        });
    })
</script>
<script>
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date(),
        todayHighlight: true,
        autoclose: true
    });
</script>
@endpush

