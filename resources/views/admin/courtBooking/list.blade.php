@extends('layouts.admin')

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">

                        @include('alertMessages')

                        <table id="courtTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Court Name</th>
                                <th>Time Schedule</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($courtBookings)
                            @foreach($courtBookings as $key => $courtBooking)

                            <tr>
                                <td>{{$key+1}}</td>

                                <td>{{$courtBooking['court']['court_name']}}</td>
                                <td>{{$courtBooking['time_schedule']['time']}}</td>
                                <td>{{$courtBooking['status']}}</td>
                                <td>
                                    <a href="" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-danger"><i class="fa fa-remove"></i></a>
                                </td>



                            @endforeach
                            @endisset                          </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $('#courtTable').DataTable({
        processing: true,


    });
</script>

@endpush



