@extends('layouts.admin')



@section('content')
<?php


if(!empty($getSetting)){

    $id = $getSetting['id'];
    $website_name = $getSetting['website_name'];
    $website_name_short = $getSetting['website_name_short'];
    $website_notice=$getSetting['website_notice'];
    $fb_url = $getSetting['fb_url'];
    $fb_page = $getSetting['fb_page'];
    $logo = $getSetting['logo'];
    $google_plus_url = $getSetting['google_plus_url'];
    $twitter_url = $getSetting['twitter_url'];
    $skype_url = $getSetting['skype_url'];
    $slogan = $getSetting['slogan'];
    $short_description = $getSetting['short_description'];
    $google_maps_iframe = $getSetting['google_maps_iframe'];
    $owner_name = $getSetting['owner_name'];
    $owner_email = $getSetting['owner_email'];
    $owner_contact = $getSetting['owner_contact'];
    $owner_address = $getSetting['owner_address'];
    $version = $getSetting['version'];
    $routeAction=route('setting.update',$id);
    $method=method_field('PUT');




}else{
    $id="";
    $website_name="";
    $website_name_short="";
    $website_notice="";
    $fb_url="";
    $twitter_url="";
    $skype_url="";
    $google_plus_url="";
    $fb_page="";
    $logo="";
    $slogan="";
    $short_description="";
    $google_maps_iframe="";
    $owner_email="";
    $owner_address="";
    $owner_name="";
    $owner_contact="";
    $version="";
    $routeAction=route('setting.store');
    $method="";
}

?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        @include('alertMessages')
                        <form action="{{$routeAction}}" method="post">
                            {{$method}}
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="website_name">Website Name</label>
                                        <input type="text" class="form-control" id="website_name " name="website_name"
                                               value="{{ $website_name }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="website_name_short">Website Short Name</label>
                                        <input type="text" class="form-control" id="website_name_short"
                                               name="website_name_short"
                                               value="{{$website_name_short }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="$website_notice">Website Notice</label>
                                <textarea class="form-control" id="$website_notice"
                                          name="website_notice" rows="5">
                                    {{ $website_notice }}
                                    </textarea>
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <input   class="form-control hidden " type="text" value="{{$logo}}" name="oldLogo">
                                    <label for="cropper_pic">Logo</label><br>
                                    <button class="btn btn-primary" onclick="$('#cropper_pic').click();" type="button">
                                        Select Image
                                    </button>
                                    <input type="file" id="cropper_pic" style="display:none">
                                    <input type="hidden" name="logo" id="inputCropperPic"
                                           value="">

                                    <div id="previewWrapper" class="positionrelative hidden">
                                        <img src="{{asset('images/'.$logo)}}" alt="" id="croppedImagePreview"
                                             class="img-thumbnail img-responsive " width="200px" height="200px"><br>
                                        <button class="btn-sm btn btn-danger" id="removeCroppedImage" type="button">
                                            Remove
                                        </button>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="fb_url">Facebook Page/Profile Url</label>
                                        <input type="text" class="form-control" id="fb_url" name="fb_url"
                                               value="{{ $fb_url }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="google_plus_url">Google Plus Url</label>
                                        <input type="text" class="form-control" id="google_plus_url" name="google_plus_url"
                                               value="{{ $google_plus_url}}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="skype_url">Skype Url</label>
                                        <input type="text" class="form-control" id="skype_url" name="skype_url"
                                               value="{{ $skype_url}}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="twitter_url">Twitter Url</label>
                                        <input type="text" class="form-control" id="twitter_url" name="twitter_url"
                                               value="{{ $twitter_url}}">
                                    </div>
                                </div>


                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="google_maps_iframe">Google Maps Iframe</label>
                                <textarea name="google_maps_iframe" id="google_maps_iframe" rows="5"
                                          class="form-control"><?= $google_maps_iframe ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="fb_page">Facebook page Iframe</label>
                                <textarea name="fb_page" id="fb_page" rows="5"
                                          class="form-control">{{ $fb_page }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="slogan">Website Slogan/Tag Line</label>
                                <textarea name="slogan" id="slogan" rows="5"
                                          class="form-control"><?= $slogan ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="short_description">Short Description</label>
                                <textarea name="short_description" id="short_description" rows="5"
                                          class="form-control"><?= $short_description ?></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="owner_name">Company/Owner Name</label>
                                        <input type="text" class="form-control" id="owner_name" name="owner_name"
                                               value="<?= $owner_name ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="owner_email">Company/Owner Email</label>
                                        <input type="email" class="form-control" id="owner_email" name="owner_email"
                                               value="<?= $owner_email ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="owner_address">Company/Owner Address</label>
                                        <input type="text" class="form-control" id="owner_address" name="owner_address"
                                               value="<?= $owner_address ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="owner_contact">Company/Owner Contact No.</label>
                                        <input type="text" class="form-control" id="owner_contact" name="owner_contact"
                                               value="<?= $owner_contact ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="version">System Version</label>
                                        <input
                                            type="text" class="form-control" id="version" name="version"
                                            value="<?= $version ?>">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="id" value="<?= $id ?>">

                            <div class="form-group">
                                <button class="btn btn-primary mr15" type="submit">Save Changes</button>
                                <button class="btn btn-danger" type="button" id="cancelBtn"
                                        onclick="window.history.back()">Cancel
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.cropper')
@endsection





@push('scripts')
<script>
    var aspectRatioInput = 6/1;
</script>
<script>
    var img = "{{$logo}}";
    if (img != "") {
        $('#previewWrapper').removeClass('hidden');
    }
</script>
@endpush