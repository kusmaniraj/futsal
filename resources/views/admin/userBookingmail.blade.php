<html>


<head>
    <title>Confirmed Booking Courts</title>

</head>
<body>
<h2>Hello {{$user->name}} Welcome to {{$getSetting['website_name']}}</h2>

<div>
    <h5>Date:&nbsp;{{date('d F Y D', strtotime($bookingCourtList['updated_at']))}}</h5>
</div>
<table style="border: none">
    <tbody>
    <tr>


        <th>Court Name</th>
        <th>Court Book Date</th>
        <th>Booking Time</th>
        <th>Price</th>

    </tr>

    @isset($bookingCourtList)
    @for($i=0; $i < count($bookingCourtList['court_names']);$i++)
    <tr>

        <td>{{$bookingCourtList['court_names'][$i]}}</td>
        <td>{{$bookingCourtList['dates'][$i]}}</td>
        <td>{{$bookingCourtList['times'][$i]}}</td>
        <td>{{$bookingCourtList['prices'][$i]}}</td>

    </tr>

    @endfor
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <th>Total Price :</th>
        <th>{{$bookingCourtList['total_price']}}</th>
    </tr>


    @endisset


    </tbody>

</table>
<div class="row">
    <h3 class="text-danger">Congrats !</h3>
    <p>Your Reserved Courts has been Confirmed,So Please Visit on Time</p>
</div>
<div style="text-align: center">
    Copyright of @ 2018 <a href="{{url('/')}}">{{$getSetting ? $getSetting['website_name'] : " Template"}}</a>
</div>
<div class="clearfix"></div>

</body>
</html>
