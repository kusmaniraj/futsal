@extends('layouts.admin')

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">

                        @include('alertMessages')

                        <table id="userTable" class="table table-bordered">
                            <thead>
                            <tr >
                                <th>SN</th>
                                <th>User Name</th>
                                <th>User Email</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($users)
                            @foreach($users as $key => $user)

                            <tr id="court{{$user['id']}}"  >
                                <td>{{$key+1}}</td>
                                <td>{{$user['name']}}</td>
                                <td>{{$user['email']}}</td>

                                <td>

                                    <a href="{{route('admin.userDelete',$user['id'])}}" class="btn btn-danger remove"  ><i class="fa fa-remove"></i></a>
                                </td>



                                @endforeach
                                @endisset                          </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.court.courtFormModal')
@endsection
@push('scripts')
<script>
    $('#userTable').DataTable({
        processing: true,


    });
</script>
<script src="{{asset('pages/admin/court.js')}}"></script>

@endpush



