@extends('layouts.admin')
@push('styles')
<link href="{{asset('pages/admin/user.css')}}" rel="stylesheet">
@endpush

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">

            <div class="x_panel">
                <div class="x_content">
                    <div class="row">

                        @include('alertMessages')

                        <table id="userCourtBookedTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Booked Date</th>
                                <th>User Name</th>
                                <th>User Email</th>
                                <th>Status</th>
                                <th>view</th>




                            </tr>
                            </thead>
                            <tbody>
                            @isset($usersCourtsBooked)
                            @foreach($usersCourtsBooked as $key => $value)

                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value['created_at']}}</td>
                                <td>{{$value['user']['name']}}</td>
                                <td>{{$value['user']['email']}}</td>

                                <td><select class="form-control userBookingStatus " id="status{{$value['id']}}"  name="status" data-status="{{$value['status']}}"   data-id="{{$value['id']}}">
                                        @if($value['status']=='Confirmed')
                                        <option value="Confirmed"  class="text-success" selected>Confirmed</option>
                                        <option value="Not_Confirmed"  class="text-danger">Not Confirmed</option>
                                        <option value="Pending">Pending</option>

                                        @elseif($value['status']=='Not_Confirmed')
                                        <option value="Not_Confirmed"  class="text-success" selected>Not Confirmed</option>
                                        <option value="Confirmed"  class="text-danger">Confirmed</option>
                                        <option value="Pending">Pending</option>
                                        @else
                                        <option value="Pending"  class="text-success" selected>Pending</option>
                                        <option value="Confirmed"  class="text-danger">Confirmed</option>
                                        <option value="Not_Confirmed"  class="text-danger">Not Confirmed</option>
                                        @endif


                                    </select></td>
                                <td><a href="#" data-id="{{$value['id']}}" class="btn btn-primary viewUserCourtBooked"><i class="fa fa-eye"></i></a></td>


                                @endforeach
                                @endisset
                            </tr>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.users.userCourtBookedModal')
@endsection
@push('scripts')
<script src="{{asset('pages/admin/user.js')}}"></script>



@endpush



