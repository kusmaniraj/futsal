
<div id="userCourtBookedModal" class="modal fade" role="dialog">
<!--    loader-->

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 ">
                        <ul class="list-unstyled user-profile ">

                        </ul>
                    </div>
                    <div class="col-md-4 ">
                        <ul class="list-unstyled bookedDate "  >


                        </ul>

                    </div>


                </div>
                <table id="viewUserCourtBookedTable" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Courts</th>
                        <th>Time</th>
                        <th>Booked Court Date</th>
                        <th>Price</th>


                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="4"><p class="pull-right">Total Price :</p></th>

                        <th id="total-price">Rs:5000</th>
                    </tr>
                    </tfoot>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button"data-id="" data-status=""  class="btn  userBookingStatus"  >Not Confirmed</button>
            </div>

        </div>

    </div>


</div>

