@extends('layouts.admin')

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">


                        <div class="col-md-6">
                            <ul>
                                <li>Start Hour:@isset($start_hour){{date("g:i A", strtotime($start_hour))}}@endisset
                                </li>
                                <li>End Hour:@isset($end_hour){{date("g:i A", strtotime($end_hour))}}@endisset</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @include('alertMessages')
                            <form action="{{route('time_schedule.store')}}" method="post"
                                  enctype=multipatr/form-data>
                                {{ csrf_field() }}


                                <div class="form-group">

                                    <label for="court_times">Court Time</label>

                                    <div class="row">
                                        <div
                                            class=" col-md-4 form-group{{ $errors->has('start_hour') ? ' has-error' : '' }}">
                                            <select name="start_hour" class="form-control ">

                                                @isset($end_hour)
                                                <option value="{{str_replace(':00','',$start_hour)}}" readonly selected>
                                                    {{date("g:i A",
                                                    strtotime($start_hour))}}
                                                </option>
                                                @else
                                                <option value="0" disabled selected>select start hour</option>
                                                @endisset
                                                @include('admin.time_schedule')


                                            </select>
                                            @if ($errors->has('start_hour'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('start_hour') }}</strong>
                                    </span>
                                            @endif
                                        </div>


                                        <div
                                            class=" col-md-4 form-group{{ $errors->has('end_hour') ? ' has-error' : '' }}">
                                            <select name="end_hour" class="form-control ">

                                                @isset($end_hour)
                                                <option value="{{str_replace(':00','',$end_hour)}}" readonly selected>
                                                    {{date("g:i A",
                                                    strtotime($end_hour))}}
                                                </option>
                                                @else
                                                <option value="0" disabled selected>select start hour</option>
                                                @endisset
                                                @include('admin.time_schedule')


                                            </select>
                                            @if ($errors->has('end_hour'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('end_hour') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                </div>


                                <div class="form-group">
                                    <button class="btn btn-primary mr15" type="submit">Save Changes</button>

                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $('#timeScheduleTable').DataTable({
        processing: true,


    });
</script>

@endpush



