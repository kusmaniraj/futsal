@extends('layouts.admin_auth')

@section('content')
<div class="login_wrapper">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <section class="login_content">
        <form class="form-horizontal" method="POST" action="{{ route('admin.password.email') }}">
            {{csrf_field()}}
            <h1>@isset($title){{$title}}@endisset </h1>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="E-mail" name="email" value="{{old('email')}}"/>
                @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>

                @endif
            </div>

            <div>
                <button class="btn btn-primary submit" type="submit">Send Password Reset Link</button>
                <a class="reset_pass" href="{{route('admin.login')}}">Back to Login</a>
            </div>

            <div class="clearfix"></div>

            <div class="separator">


                <div class="clearfix"></div>
                <br/>

                <div>
                    <h1><i class="fa fa-paw"></i> {{$getSetting ? $getSetting['website_name'] : " Template"}}
                    </h1>

                    <p>©2018 All Rights Reserved.{{$getSetting ? $getSetting['website_name'] : " Template"}}
                        Privacy and Terms</p>
                </div>
            </div>
        </form>
    </section>
</div>
@endsection
