@extends('layouts.admin_auth')

@section('content')

<div class="login_wrapper">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <section class="login_content">
        <form class="form-horizontal" method="POST" action="{{ route('admin.register') }}">
                                 {{ csrf_field() }}
            <h1>@isset($title){{$title}}@endisset Form</h1>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{old('name')}}"/>
                @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>

                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="E-mail" name="email" value="{{old('email')}}"/>
                @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>

                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password"/>
                @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>

                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation"/>

            </div>
            <div>
                <button class="btn btn-primary " type="submit" >Submit</button>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
                <p class="change_link">Already a member ?
                    <a href="{{route('admin.login')}}" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br/>

                <div>
                    <h1><i class="fa fa-paw"></i> {{$getSetting ? $getSetting['website_name'] : " Template"}}
                    </h1>

                    <p>©2018 All Rights Reserved.{{$getSetting ? $getSetting['website_name'] : " Template"}}
                        Privacy and Terms</p>
                </div>
            </div>
        </form>
    </section>
</div>

@endsection
