@extends('layouts.web')

@section('content')
<div class=" web-content">
    <div class=" col-md-12 center-content">
        <div class="content-header ">
            <h5>Login <span class="v-line"></span> <a style="font-weight: bold; color:#fff" href="{{ url()->previous() }}" class="push-right"><i class="fa fa-arrow-left"></i>Back</a></h5>


        </div>
        <div class="container " >
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                               required
                               autofocus>

                        @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember
                                Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary  loginBtn">
                            Login
                        </button>

                        <a class="btn btn-link forgotPwdLink" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                        <a class="btn btn-link forgotPwdLink " href="{{ route('register') }}">
                            New Registration
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
