@extends('layouts.web')

@section('content')
<div class=" web-content">
    <div class=" col-md-12 center-content">
        <div class="content-header ">
            <h5>Forgot Password <span class="v-line"></span> <a style="font-weight: bold; color:#fff" href="{{ url()->previous() }}" class="push-right"><i class="fa fa-arrow-left"></i>Back</a></h5>


        </div>
        <div class=row"  >
            <div class="container" style="width: 100%">

                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary sendPwdLink" >
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection
