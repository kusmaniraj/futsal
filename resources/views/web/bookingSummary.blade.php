<div id="bookingSummaryModal" class="modal fade" role="dialog">


    <div class="modal-dialog modal-md">
        <div class="loader " style="display: none" id="ajax-loader-bookCourt"></div>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Booking Summary</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" id="bookingForm">
                            {{csrf_field()}}
                            <div class=" col-md-6 bookSummary-left">
                                <div id="selectedCourtHeader">
                                    <h5>Selected Court :</h5>
                                </div>
                                <div id="selectedCourtList">





                                </div>
                                <div class="col-md-12 court-total-price">
                                    <p class="pull-right">
                                        Total :Rs 3000
                                    </p>

                                </div>

                            </div>
                            <div class="bookSummary-right ">
                                <div id="userHeader">
                                    <h5>Your Details :</h5>
                                </div>
                                <p><span class="text-danger">*</span>Arrive at least ten minutes before start of your booking.
                                    Your personal data is only for provider’s internal use.<span class="text-danger">*</span></p>
                                <table class="bookingTable">
                                    <tbody>

                                    <tr>
                                        <th>Name:</th>
                                        <td class="userName"</td>
                                    </tr>


                                    <tr>
                                        <th>E-mail:</th>
                                        <td class="userEmail"></td>
                                    </tr>

                                    <tr>
                                        <th>Note:</th>
                                        <td><textarea name="note" id="" cols="30" rows="5"></textarea><p class="noteError  text-danger hidden"></p></td>
                                    </tr>
                                    <tr>

                                        <td colspan="2"><input  type="checkbox" id="terms" name="terms"
                                                                value="1">&nbsp;I accept the <a class="showTermsDialog" href="#">terms</a>
                                            of the facility
                                            <p class="termsError text-danger hidden"></p>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td class="right">
                                            <p class="bookingSummaryBtn">

                                                <a class=" btn btn-primary  bookingBtn  " type="button"  data-dismiss="modal" href="#"><i
                                                        class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;Back</a>
                                                <button type="submit" class=" btn btn-primary  bookingBtn" href="#">Send booking&nbsp;<i
                                                        class="fa fa-check-circle" aria-hidden="true"></i></button>

                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>

                        </form>



                    </div>

                </div>


            </div>

        </div>

    </div>
    <script>

    </script>
</div>