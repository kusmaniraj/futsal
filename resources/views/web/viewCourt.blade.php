@extends('layouts.web')
@section('content')
<style>

</style>
<div class=" web-content">
    <div class="col-md-3 ">
        <div class=" col-md-12 left-content">
            <div class="content-header ">
                <h5>Search</h5>
            </div>


        </div>
        <div class="col-md-12">
            <form class="form-horizontal">

                <div class="control-group">
                    <label class="control-label" for="name">Name:</label>

                    <div class="controls">
                        <input id="name" name="name" class="form-control" type="text"
                               placeholder="Enter Futsal Name" class="input-large" required="">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="address">Address:</label>

                    <div class="controls">
                        <input id="address" name="address" class="form-control" type="text"
                               placeholder="Enter Adress" class="input-large" required="">
                    </div>
                </div>

                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="searchFutsal"></label>

                    <div class="controls">
                        <button id="searchFutsal" name="" class="btn btn-success form-control">Find</button>
                    </div>
                </div>


            </form>
        </div>

    </div>
    <div class="col-md-9 ">
        <div class=" col-md-12 center-content">
            <div class="content-header ">
                <h5>Futsal Court Booking</h5>
            </div>
            <div class="col-md-12">
                <form>
                    <div class="col-sm-12">
                        <div class="col-md-5">
                            <div class="control-group">

                                <div class="controls form-inline">
                                    <label class="control-label " for="schedule">Schedule For</label>
                                    <input required="" id="schedule" name="schedule" type="date"
                                           class=" form-control " value="{{ date('Y-m-d')}}"
                                           min="{{ date('Y-m-d')}}">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="bookinglegend">
                                <ul>
                                    <li>
                                        <div class="bookingstatus0"></div>
                                        Available
                                    </li>
                                    <li>
                                        <div class="bookingstatus2"></div>
                                        Unavailable
                                    </li>
                                    <li>
                                        <div class="bookingstatus1"></div>
                                        Booked
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12  bookingCourt">
                        <div id="result"></div>


                        <table id="courtTable">
                            <tr class="court">
                                <th>Hour</th>
                                <th>Court 1</th>
                                <th>Court 2</th>
                                <th>Court 3</th>
                                <th>Court 4</th>
                                <th>Court 5</th>

                            </tr>


                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>
                            <tr class="courtBooking">
                                <th>7:0am-8:00am</th>
                                <td><input type="checkbox"  name="courtBook[]"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="unavailableCourt"></td>
                                <td><input type="checkbox" name="courtBook[]"></td>
                                <td class="bookedCourt"></td>




                            </tr>

                            <tr class="price">
                                <th>Price</th>
                                <th>Rs1400</th>
                                <th>Rs1400</th>
                                <th>Rs1400</th>
                                <th>Rs1400</th>
                                <th>Rs1400</th>

                            </tr>
                            <tr>
                                <th>
                                    <button id="proceedButton" type="button" class="btn btn-primary" >Proceed</button>
                                </th>
                            </tr>





                        </table>
                    </div>




                </form>


            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $("#courtTable td").click(function () {

            var column_num = parseInt($(this).index()) ;
            var row_num = parseInt($(this).parent().index());
            var time=$(this).data('time');

            $("#result").html(time);
        });
    });
</script>
@endpush