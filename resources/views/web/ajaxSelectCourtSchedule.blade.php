<?php
$start_time = "";
$end_time = "";
$date = "";
?>
@isset($court_schedule['date'])
@php
$date = date('d F Y D', strtotime($court_schedule['date']));
@endphp
@endisset
@isset($court_schedule['start_hour'])
<?php

$start_time = date('g:i A', strtotime($court_schedule['start_hour']));

$end_time = date('g:i A', strtotime($court_schedule['end_hour']));
?>
@endisset

<div class=" col-md-12 center-content">
    <div class="loader " style="display: none" id="ajax-loader-courtTable"></div>
    <div class="content-header ">
        <h5>Futsal Court
            <div class="pull-right">

                <a id="prevDate"><i class="fa fa-angle-double-left paginateLeftArrow"></i>Prev</a><span
                    class="paginateLine"></span>
                <a id="nextDate">Next<i class="fa fa-angle-double-right paginateRightArrow"></i></a>
            </div>

        </h5>
    </div>
    <div class="row">


        <div class="col-sm-12">
            <div class="col-md-5" id="bookingDate">
                <h5 style="font-weight: bold">{{$date}}&nbsp( {{$start_time}} - {{$end_time}})</h5>
            </div>

            <div class="col-md-7">
                <div class="bookinglegend">
                    <ul>
                        <li>
                            <div class="bookingstatus0"></div>
                            Available
                        </li>


                        <li>
                            <div class="bookingstatus1"></div>
                            Booked
                        </li>
                        <li>
                            <div class="bookingstatus2"></div>
                            Unfinished Booking
                        </li>
                        @if(Auth::guard('web')->check())

                        <li>
                            <div class="bookingstatus3"></div>
                            <a href="{{url('/bookingCourtList')}}">My Reservation</a>

                        </li>

                        @endif
                    </ul>

                </div>
            </div>
        </div>

        <div class="col-md-12  " id="bookingCourt">
            @empty($court_schedule['courts'])
            <h5>No Court Schedule on {{ date('d F Y D', strtotime($court_schedule['date']))}}</h5>
            @endempty


            <table id="courtTable">


                @isset($time_schedules_price)
                <tr class="courtBooking">
                    <th style="background: #008080; font-size: 12px;">Court/Time</th>


                    @foreach($time_schedules_price as $time)
                    @php
                    $start_time= date('g:i A', strtotime($time['start_hour']));
                    $end_time=date('g:i A', strtotime($time['end_hour']));
                    @endphp


                    <th>{{$start_time.'-'.$end_time}}</th>
                    @endforeach


                </tr>
                @endisset

                @isset($court_schedule['courts'])
                @foreach($court_schedule['courts'] as $court)
                <tr class="court">

                    <th>{{$court['court_name']}}</th>
                    @foreach($court['time_schedule_courts'] as $time_schedule_court)


                    @if($time_schedule_court['status']=='available')
                    <td><a title="Available Court" class="availableCourt"
                           id="court{{$time_schedule_court['id']}}"
                           data-court_book="{{$time_schedule_court['id']}}"
                           data-date="{{$court_schedule['date']}}"></a></td>


                    @elseif($time_schedule_court['status']=='booked')
                    <td><a title="Booked Court" class="bookedCourt" id="court{{$time_schedule_court['id']}}"
                           data-court_book="{{$time_schedule_court['id']}}"></a></td>
                    @else
                    @if(Auth::guard('web')->check())
                    <td><a title="Go to Yor Reserve  Court List" class="reservedCourt"  href="{{url('/bookingCourtList')}}"></a></td>

                    @else
                    <td><a title="Reserved Court" class="reservedCourt"
                           id="court{{$time_schedule_court['id']}}"
                           data-court_book="{{$time_schedule_court['id']}}"></a></td>

                    @endif

                    @endif
                    @endforeach


                </tr>
                @endforeach
                @endisset
                @isset($time_schedules_price)
                <tr class="price">
                    <th style="background: #008080;  font-size: 12px;">Price</th>

                    @foreach($time_schedules_price as $price)
                    <th style="font-size: 10px;">Nrs.{{$price['price']}}</th>
                    @endforeach


                </tr>
                @endisset


            </table>
        </div>


    </div>
</div>