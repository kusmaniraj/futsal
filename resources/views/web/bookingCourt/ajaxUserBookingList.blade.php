
<table id="courtBookingTable" class="table table-bordered">

    <thead>
    <tr style="color:#fff;background:#000">
        <th>SN</th>
        <th>Booked At</th>
        <th>Court Name</th>
        <th>Price</th>
        <th>Booked Date</th>
        <th>Booked Time</th>
        <th>Status</th>
        <th>Action</th>

    </tr>
    </thead>
    <tbody>


    @isset($bookingCourtLists)
    @foreach($bookingCourtLists as $key=> $bookingCourtList)
    @php
    $courtNames=$bookingCourtList['courtNames'];

    $dates=$bookingCourtList['dates'];
    $times=$bookingCourtList['times'];
    @endphp
    <tr id="booked{{$bookingCourtList['id']}}">
        <td>{{$key +1}}</td>
        <th>{{$bookingCourtList['updated_at']}}</th>
        <td>
            @for ($i = 0; $i < count($courtNames); $i++)
            <span class="label label-default">{{$courtNames[$i]}}</span>
            &nbsp;@endfor
        </td>
        <th>
            Rs:{{$bookingCourtList['total_price']}}

        </th>
        <td>
            @for ($i = 0; $i < count($dates); $i++)
            <span class="label label-default">{{$dates[$i]}}</span>
            &nbsp;@endfor
        </td>
        <td>
            @for ($i = 0; $i < count($times); $i++)
            <span class="label label-default">{{$times[$i]}}</span>
            &nbsp;@endfor
        </td>

        <td><span class="label label-{{($bookingCourtList['status']=='Not_Confirmed')? 'danger':'success'}}">{{$bookingCourtList['status']}}</span>
        </td>
        <td>

            <a data-id="{{$bookingCourtList['id']}}"
               class="printBookedCourt" style="color: #000" title="Print"><i class="fa fa-print text-info"></i></a>
            &nbsp;
            <a data-id="{{$bookingCourtList['id']}}"
               class=" removeBookedCourtBtn"  style="color:red" title="Cancel"><i class="fa fa-remove"></i></a>
        </td>
        @endforeach
    </tr>
    @endisset


    </tbody>

</table>


