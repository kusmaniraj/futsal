@extends('layouts.web')


@push('styles')
<link rel="stylesheet" href="{{asset('pages/web/bookingCourt/bookingCourt.css')}}">
@endpush
@section('content')
<div class=" web-content">


    <div class=" container center-content">
        <div class="content-header ">
            <h5>My Bookings <span style="background: #000" class="badge">{{$countBooking}}</span> <span
                    class="v-line"></span> <a style="font-weight: bold; color:#fff" href="{{ url()->previous() }}"
                                              class="push-right"><i
                        class="fa fa-arrow-left""></i>Back</a>
                <a href="{{url('/')}}" class="pull-right"><i class="fa fa-calendar" style="color: #000;"></i>Time
                    Schedule</a>
            </h5>


        </div>

        <div class="col-md-12">
            @include('alertMessages')

            <p class="userFilterReservation">
                <input checked="checked" type="radio" name="userResListType" value="new" id="new">
                <label for="new">Current</label>
                <input type="radio" name="userResListType" value="old" id="old">
                <label for="old">Previous bookings</label>
            </p>
            <div class="loader " style="display: none" id="ajax-loader-userReserveTable"></div>

            <div id="userReservePage">


                <table id="courtBookingTable" class="table table-bordered">

                    <thead>
                    <tr style="color:#fff;background:#000">
                        <th>SN</th>
                        <th>Booked At</th>
                        <th>Court Name</th>
                        <th>Price</th>
                        <th>Booked Date</th>
                        <th>Booked Time</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>


                    @isset($bookingCourtLists)
                    @foreach($bookingCourtLists as $key=> $bookingCourtList)
                    @php
                    $courtNames=$bookingCourtList['courtNames'];

                    $dates=$bookingCourtList['dates'];
                    $times=$bookingCourtList['times'];
                    @endphp
                    <tr id="booked{{$bookingCourtList['id']}}">
                        <td>{{$key +1}}</td>
                        <th>{{$bookingCourtList['updated_at']}}</th>
                        <td>
                            @for ($i = 0; $i < count($courtNames); $i++)
                            <span class="label label-default">{{$courtNames[$i]}}</span>
                            &nbsp;@endfor
                        </td>
                        <th>
                            Rs:{{$bookingCourtList['total_price']}}

                        </th>
                        <td>
                            @for ($i = 0; $i < count($dates); $i++)
                            <span class="label label-default">{{$dates[$i]}}</span>
                            &nbsp;@endfor
                        </td>
                        <td>
                            @for ($i = 0; $i < count($times); $i++)
                            <span class="label label-default">{{$times[$i]}}</span>
                            &nbsp;@endfor
                        </td>

                        <td><span class="label label-{{($bookingCourtList['status']=='Not_Confirmed')? 'danger':'success'}}">{{$bookingCourtList['status']}}</span>
                        </td>
                        <td>

                            <a data-id="{{$bookingCourtList['id']}}"
                               class="printBookedCourt" style="color: #000" title="Print"><i class="fa fa-print text-info"></i></a>
                            &nbsp;
                            <a data-id="{{$bookingCourtList['id']}}"
                               class=" removeBookedCourtBtn"  style="color:red" title="Cancel"><i class="fa fa-remove"></i></a>
                        </td>
                        @endforeach
                    </tr>
                    @endisset


                    </tbody>

                </table>



            </div>

        </div>

    </div>
</div>


@include('web/bookingCourt/userCourtBookedModal')
@endsection
@push('scripts')
<script src="{{asset('pages/web/bookingCourt/PrintArea.js')}}"></script>


<script src="{{asset('pages/web/bookingCourt/bookingCourt.js')}}"></script>



@endpush

