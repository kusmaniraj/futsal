<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$getSetting ? $getSetting['website_name'] : ""}}| @isset($title){{$title}}@endisset</title>
    <!-- Bootstrap -->
    <link href="{{asset('futsal/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('futsal/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<!--    datepicker-->
    <link rel="stylesheet" href="{{asset('futsal/datePicker/bootstrap-datepicker.css')}}">
<!--    build css-->
    <link rel="stylesheet" href="{{asset('futsal/css/futsal.css')}}">
<!--    thubmnail-->
    <link rel="stylesheet" href="{{asset('futsal/css/thumbnail.css')}}">

<!--    auth css-->
    <link rel="stylesheet" href="{{asset('pages/web/auth/auth.css')}}">
    @stack('styles')
    <link href="{{asset('futsal/datatables/datatables.css')}}" rel="stylesheet">




    <!-- jQuery -->
    <script src="{{asset('futsal/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('futsal/bootstrap/dist/js/bootstrap.min.js')}}"></script>







</head>