<div class="header">


    <div class="row top-header">
        <div class=" col-md-6 headerContact">
            <p>Contact: {{$getSetting ? $getSetting['owner_contact'] : ""}} | E-mail:
                <a href="www.futsal.com">{{$getSetting ? $getSetting['owner_email'] : ""}}</a>
            </p>
        </div>
        <div class=" col-md-4 pull-right login-page ">


            @if(Auth::guard('web')->check())
            <div class=" col-md-  logout-panel">
                <div class="col-md-8">
                    <ul style="list-style-type: none">
                        <li>
                            <a href="#"><i class="fa fa-user"></i>{{Auth::user()->name}}</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-chevron-right"></i>My Bookings </a>
                        </li>
                    </ul>

                </div>
                <div class="col-md-4">
                    <a id="logoutBtn" href="{{route('logout')}}"> Logout <i class="fa fa-unlock"></i> </a>
                </div>


            </div>
            @else
            <div id="loginCollapse" class="collapse">


                <form id="userLoginForm" method="POST">
                {{ csrf_field() }}
                <table class="userLoginTable">
                    <tbody>
                    <tr>
                        <td><label for="email">E-mail:</label></td>
                        <td><input class="text " type="email" name="email" value="{{old('email')}}" required></td>


                    </tr>

                    <tr class="emailErrorMsg " style="display: none">
                        <td colspan="2">

                        </td>
                    </tr>

                    <tr>
                        <td><label for="email">Password:</label></td>
                        <td><input class="text " type="password" name="password" value="" required></td>

                    </tr>

                    <tr class="passwordErrorMsg " style="display: none">
                        <td colspan="2" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td class="tdRemember"><input type="checkbox" name="loginRemember" id="loginRemember"
                                                      value="{{ old('remember') ? 'checked' : '' }}">&nbsp;<label
                                for="loginRemember">Remember me</label>
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td class="userLoginTd">
                            <button class="userLoginSubmit" type="submit">Login<i
                                    class="fa fa-arrow-circle-right"></i>
                            </button>


                        </td>
                    </tr>
                    </tbody>
                </table>


                <div class="loginLinks"><p><i class="fa fa-user"></i><a rel="nofollow"
                                                                        href="{{ route('register') }}">New
                            registration</a></p>

                    <p><i class="fa fa-lock"></i><a rel="nofollow" href="{{ route('password.request') }}">Forgotten
                            Password?</a>
                    </p></div>
                </form>

            </div>
            <div class=" col-md-4 login-panel">

                <a id="loginBtn"> Login<i class="fa fa-plus-circle fa-2 collapseIcon"></i>
                </a>


            </div>

            @endif


        </div>

    </div>


    <div class="row " >
        <div class="col-md-3 logo">
            <a href="" class="">
                <img src="{{asset('futsal/images/header.png')}}" class=" logo-image">
            </a>

        </div>
        <div class="col-md-9 ">

            <div class="row">
                <div class="nav-menu">
                    <ul class="nav nav-tabs ">
                        <li class="nav-item">
                            <a class="nav-link {{$title=='Home' ? 'active' : ' '}}" href="{{url('/')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Location</a>
                        </li>
                        @if(Auth::guard('web')->check())
                        <li class="nav-item">
                            <a class="nav-link {{$title=='My Booking Court' ? 'active' : ' '}}"
                               href="{{url('/bookingCourtList')}}">My Reservations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$title=='Account Setting' ? 'active' : ' '}}"
                               href="{{url('/accountSetting')}}">Account Setting</a>
                        </li>
                        @endif


                    </ul>
                </div>
            </div>


        </div>
    </div>



</div>
<script>


</script>

