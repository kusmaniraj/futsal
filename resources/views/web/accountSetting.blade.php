<?php
if(isset($user)){
    $id=$user->id;
    $email=$user->email;
    $name=$user->name;
    $password=$user->password;
}
?>
@extends('layouts.web')


@section('content')
<div class=" web-content">
    <div class=" container center-content">
        <div class="content-header ">
            <h5>Account Setting <span class="v-line"></span> <a style="font-weight: bold; color:#fff" href="{{ url()->previous() }}" class="push-right"><i
                        class="fa fa-arrow-left""></i>Back</a></h5>


        </div>
        <div class="col-md-12">
            @include('alertMessages')
            <form class="form-horizontal" method="POST" action="{{ route('web.updateAccountSetting') }}">
                <input type="hidden" value="{{$id}}" name="id">
                <div class="col-md-offset-4">
                    <h5 class="text-info">*Current Password is needed for Account  Setting Change*</h5>


                </div>

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ $name }}" required
                               autofocus>

                        @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email }}"
                               required>

                        @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ Session::get('c_password')|| $errors->has('c_password')  ? ' has-error' : '' }}">
                    <label for="c_password" class="col-md-4 control-label"> Current Password</label>

                    <div class="col-md-6">
                        <input id="c_password" type="password" class="form-control" name="c_password" >
                        @if ($errors->has('c_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('c_password') }}</strong>
                                    </span>
                        @endif

                        @if (Session::get('c_password'))
                                    <span class="help-block">
                                        <strong>{{ Session::get('c_password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label"  >New Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password"d>

                        @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                              d>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4  ">
                        <button type="submit" class="btn btn-primary registerBtn">
                            Save Account
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
