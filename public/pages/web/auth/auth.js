var auth_module = new auth();
function auth() {


    var $self = this;



    this.bindFunction = function () {

        $('#loginBtn').click(function (e) {
            e.preventDefault();
            $('#loginCollapse').toggle(500);
            $(".collapseIcon", this).toggleClass("  fa-minus-circle fa-plus-circle");


        })

        $('#userLoginForm').on('submit',function(e){
            e.preventDefault();
            var selectorForm='userLoginForm';
            $self.login(selectorForm);
        })
        $('#userLoginForm2').on('submit',function(e){

            e.preventDefault();

            var selectorForm='userLoginForm2';
            $self.login(selectorForm);
        })







    }
    this.login=function(selectorForm){
        $('#ajax-loader-courtTable').show();
        var formData=$('#'+selectorForm).serialize();
        new ajaxHelper([base_url + "/ajaxLogin",formData, "post", ""]).makeAjaxCall().then(function (data) {
            if (data.errors) {

                $self.printValidatorMessage(selectorForm,data.errors);
            }else{
               window.location.href=base_url+'/login';
            }
            $('#ajax-loader-courtTable').hide();

        }, function (err) {
            var data = JSON.parse(err.responseText);


            if (data.errors) {

                $self.printValidatorMessage(selectorForm,data.errors);
            }
        });


    }
    this.printValidatorMessage = function (selectorForm,errors) {
        if (errors) {
            if(selectorForm=='userLoginForm2'){
                $.each(errors, function (key, value) {

                    $('#'+selectorForm+' .'+key+'ErrorMsg').show();
                    $('#'+selectorForm+' .'+key+'ErrorMsg p').text(value);
                });

            }else{
                $.each(errors, function (key, value) {

                    $('#'+selectorForm+' .'+key+'ErrorMsg').show();
                    $('#'+selectorForm+' .'+key+'ErrorMsg td').text(value);
                });
            }
            $('#ajax-loader-courtTable').hide();





        }
    }




    this.init = function () {


        $self.bindFunction();

    }();

}

