var booking_module = new bookingCourt();
function bookingCourt() {


    var $self = this;
    var unfinishedBooked = [];
    var totalPrice=0;
    var price=0;
    var CourtReservedTable="";

    this.CourtDataTables=function(){
        $self.CourtReservedTable=$('#courtBookingTable').DataTable();
    }

    this.bindFunction = function () {

        //DataTables

        $self.CourtDataTables();
        //end DataTables


        $(document).on('click', '#courtTable .availableCourt', function (e) {
            e.preventDefault();
            var book_id = $(this).data('court_book');
            var price = parseInt($(this).data('price'));
            unfinishedBooked.push(book_id);
            $self.selectCourt(book_id,price)




        })
        $(document).on('click', '#courtTable .unfinishedCourt', function (e) {
            e.preventDefault();
            var book_id = $(this).data('court_book');
            var price = parseInt($(this).data('price'));
            unfinishedBooked.pop(book_id);
            $self.removeSelectedCourt(book_id,price)
        });
        $("#selectedCourtTable").on('click', '.removeSelectedCourt', function (e) {
            e.preventDefault();
            var book_id = $(this).data('id');
            var price = parseInt($(this).data('price'));

            if (confirm('Are You Sure Want To Cancel !')) {
                $self.removeSelectedCourt(book_id,price);


            } else {
                return false;
            }

        });


        $('#datepicker').datepicker({

            startDate: new Date(),
            todayHighlight: true,
        }).datepicker('setDate', new Date()).on('changeDate', function (e) {
            e.preventDefault();
            var bookingDate = $(this).datepicker('getDate');
            var date = moment(bookingDate).format('YYYY-MM-DD');
            $self.selectCourtSchedule(date);


        });

        $('#selectCourtSchedule').on('click', '#nextDate', function (e) {
            e.preventDefault();

            var date = $('#datepicker').datepicker('getDate');
            var nextHighlightDate = (date.getMonth() + 1) + '/' + (date.getDate() + 1) + '/' + date.getFullYear();
            $('#datepicker').datepicker('setDate', nextHighlightDate);
            var nextDate = moment(nextHighlightDate).format('YYYY-MM-DD');
            $self.selectCourtSchedule(nextDate);

        })
        $('#selectCourtSchedule').on('click', '#prevDate', function (e) {
            e.preventDefault();
            var date = $('#datepicker').datepicker('getDate');
            if (date > new Date()) {
                var prevHighlightDate = (date.getMonth() + 1) + '/' + (date.getDate() - 1) + '/' + date.getFullYear();
                $('#datepicker').datepicker('setDate', prevHighlightDate);
                var prevDate = moment(prevHighlightDate).format('YYYY-MM-DD');
                $self.selectCourtSchedule(prevDate);
            }


        })
        $('#continueBookingBtn').on('click', function (e) {
            e.preventDefault();
            $('#bookingSummaryModal').modal('show');
            $('#bookingForm .noteError').text('').addClass('hidden');
            $('#bookingForm .termsError').text('').addClass('hidden');;
            $('#bookingForm textarea[name="note"]').val('');
            $('#bookingForm input[name="terms"]').attr('checked',false);




        });
        $('#bookingForm').on('submit', function (e) {
            e.preventDefault();
           $self.createBookingCourt();


        });

        $(document).on('click','#courtBookingTable .removeBookedCourtBtn', function (e) {

            e.preventDefault();
            var id = $(this).data('id');

            var ask_permission = confirm("Are you sure want to Cancel  Reservation ?");
            if (ask_permission == true) {
                $self.removeCourt(id);

            }
            else {
                return false;
            }


        })

        //    userReservationTypeList
        $(document).on('change','.userFilterReservation input[name="userResListType"]', function (e) {
            e.preventDefault();
            var type = $(this).val();
            $self.userReservationTypeList(type);
        })
        //print Court
        $(document).on('click','#courtBookingTable .printBookedCourt',function(e){
            e.preventDefault();
            var id=$(this).data('id');
            $('#userCourtBookedModal').modal('show');



            $self.viewUserCourtsBooked(id);

        });

    //    printBookedCourt
        $('#userCourtBookedModal .printBookedCourtBtn').on('click',function(e) {
            e.preventDefault();
            $('#userCourtBookedModal .modal-footer').css('display',"none");
            $('#userCourtBookedModal .user-profile').css({
                'text-align': 'center','font-weight':'bold'})
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {mode: mode, popClose: close};
            $("#printCourt").printArea(options);
            $('#userCourtBookedModal .modal-footer').show();
        });


    }


    $self.selectCourt = function (book_id,price) {
        $('#ajax-loader-selectCourt').show();
        var bookingDate = $('#datepicker').datepicker('getDate');
        var date = moment(bookingDate).format('YYYY-MM-DD');


        var formData = {book_id: book_id, _token: $('meta[name="csrf-token"]').attr('content')};
        $.ajax({
            url: base_url + '/ajax_selected_court/' + book_id,
            type: 'POST',
            data: formData,

            success: function (data) {

                $('#courtTable #court' + book_id).css({'background': 'yellow'});
                $('#selected_booking_court').removeClass('hidden');
                var rowSelectedCourt = '<tr class="selectedHeader' + data.id + '">' +
                    '<th colspan="3">' + data.court.court_name + '</th>' + '</tr>';
                rowSelectedCourt += '<tr class="selectedCourt' + data.id + '">' +
                    '<td class="unfinishedBooked"><input type="hidden" value="' + data.id + '" name="book_id[]"></td>' +
                    '<td><input type="hidden" value="' + date + '" name="date">' + date +
                    '<div >' + data.time + '</div>' +
                    '</td>' +
                    '<td> Rs:' + data.price + '</td>' +
                    '<td><a title="Cancel" class=" text-danger removeSelectedCourt remove" data-price="'+data.price+'"  data-id="' + data.id + '"><i class="fa fa-trash "></i></a>' +
                    '</td>' +
                    '</tr>';
                $('#selectedCourtTable tbody').append(rowSelectedCourt);
                //append row in Booking summary form

                var rowSelectedCourtForForm = [' <div class="courtSelect" id="courtSelected'+data.id+'">'+
                                                     '<div class = "court-title " >'+ data.court.court_name + '</div>'+
                                                    '<div class = "court-body " >'+
                                                        '<input type="hidden" value="' + date + '" name="date">'+
                                                        '<input type="hidden" value="' + data.id + '" name="book_id[]">'+
                                                        '<input type="hidden" value="' + data.price + '" name="price">'+
                                                         '<div class = "col-md-12" >'+
                                                              '<div class = "col-md-8" >'+
                                                                    '<div>'+
                                                                       '<p >'+ date+ '</p >'+
                                                                    '</div>'+
                                                                    '<div>'+
                                                                     '<p >'+ data.time + ' </p>'+
                                                                    '</div>'+

                                                              '</div>'+
                                                             '<div class = "col-md-4">'+
                                                                  '<p > Rs'+ data.price +'</p>'+
                                                                '</div>' +
                                                         ' </div>' +
                                                    ' </div>' +
                                                '</div>'];

                $('#bookingSummaryModal #selectedCourtList').prepend(rowSelectedCourtForForm);
                ////add price
                totalPrice=(totalPrice + price );


                $('#bookingSummaryModal ').find('.court-total-price p').text('Total Price :'+ totalPrice);

                //user details
                $('#bookingSummaryModal .bookingTable td.userName ').text(data.user.name);
                $('#bookingSummaryModal .bookingTable td.userEmail ').text(data.user.email);

                //end append row in Booking summary form

                $('#courtTable #court' + book_id).removeClass('availableCourt');
                $('#courtTable #court' + book_id).addClass('unfinishedCourt');

                $('#courtTable #court' + book_id).attr('title', 'Unfinished Court')
                $('#ajax-loader-selectCourt').hide();
            },
            error: function () {
                $('#ajax-loader-selectCourt').hide();
                alert('error');


            }


        })

    }
    $self.removeSelectedCourt = function (book_id,price) {
        $('#ajax-loader-selectCourt').show();
        $('#courtTable #court' + book_id).css({'background': '#D1E3E9'})
        $('.selectedHeader' + book_id + ',.selectedCourt' + book_id).remove();
        //bookingSummaru Court Selected Remove
        $('#bookingSummaryModal #selectedCourtList').find('#courtSelected'+book_id).remove();
        $('#courtTable #court' + book_id).removeClass('unfinishedCourt');
        $('#courtTable #court' + book_id).addClass('availableCourt');
        $('#courtTable #court' + book_id).attr('title', 'Available Court');
        if ($('#selectedCourtTable tr').length == 0) {

            $('#selected_booking_court').addClass('hidden');
        }
        //    less Total price
        totalPrice=(totalPrice-price);
        $('#bookingSummaryModal ').find('.court-total-price p').text('Total Price :'+ totalPrice);

        $('#ajax-loader-selectCourt').hide();

    }

    $self.removeCourt = function (id) {
        $('#ajax-loader-courtTable').show();


        new ajaxHelper([base_url + "/removeCourt/" + id, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data.error) {


                $('#error-msg-alert').show();
                $('#error-msg-alert strong').text('Successfully !' + data.error);
                $('#ajax-loader-courtTable').hide();
                $('#error-msg-alert').hide().show(2000, 500).slideUp(500, function () {
                    $("#success-alert").slideUp(500);
                });

            } else {
                $self.CourtReservedTable.destroy();

                $('#courtBookingTable #booked' + id).remove();
                $("#courtBookingTable tbody  tr").each(function (key) {
                    $(this).find("td:first").text(key+1);

                });
                $self.CourtDataTables();

                $('#success-msg-alert strong').text('Successfully !' + data.success);
                $('#ajax-loader-courtTable').hide();
                $('#success-msg-alert').show().fadeTo(2000, 500).slideUp(500, function () {
                    $("#success-alert").slideUp(500);
                });

            }

        });

    }

    $self.selectCourtSchedule = function (date) {

        $('#ajax-loader-courtTable').show();
        new ajaxHelper([base_url + "/ajaxSelectCourtSchedule" + "/" + date, "", "get", ""]).makeAjaxCall().then(function (data) {

            $('#selectCourtSchedule').html(data);
            for (var i = 0; i < unfinishedBooked.length; i++) {

                $('#courtTable #court' + unfinishedBooked[i]).removeClass('availableCourt')
                $('#courtTable #court' + unfinishedBooked[i]).addClass('unfinishedCourt');
                $('#courtTable #court' + unfinishedBooked[i]).css({'background': 'yellow'})
            }
            $('#ajax-loader-courtTable').hide();
        });

    }


    this.createBookingCourt = function () {
        $('#ajax-loader-bookCourt').show();

        var formData = $('#bookingForm').serialize();
        new ajaxHelper([base_url + "/bookingCourt", formData, "post", ""]).makeAjaxCall().then(function (data) {

            if (data.error) {
                $('#bookingForm .noteError').removeClass('hidden').text( data.error.note);
                $('#bookingForm .termsError').removeClass('hidden').text(data.error.terms);



            } else {
                $('#selectCourtSchedule').html(data);
                $('#bookingSummaryModal').modal('hide');
                //remove selected Booking Court
                $('#selectedCourtTable tbody').html('');
                $('#selected_booking_court').addClass('hidden');
                $('#bookingSummaryModal #selectedCourtList').html('');
            //    end remove

            }
            $('#ajax-loader-bookCourt').hide();



        });
    }
    //userReservationTypeList
    this.userReservationTypeList = function (type) {
        $('#ajax-loader-userReserveTable').show();
        new ajaxHelper([base_url + "/userReservationTypeList/" + type, "", "get", ""]).makeAjaxCall().then(function (data) {

            $('#userReservePage').html(data);
            $self.CourtDataTables();
            $('#ajax-loader-userReserveTable').hide();
        })
    }
    //print Court
    this.viewUserCourtsBooked=function(id){
        $('#ajax-loader-printCourt').show();


        new ajaxHelper([base_url + "/showUserCourtBooked/"+id, "", "get", ""]).makeAjaxCall().then(function (data) {
            var userProfile ='<li><i class="fa fa-user"></i>&nbsp;'+data.user.name+' </li>'+
                '<li> <i class="fa fa-envelope"></i>&nbsp;'+data.user.email+' </li>' +
                '<li> <i class="fa fa-building"></i>&nbsp;Inacho, Bhaktapur </li>'+
                '<li> <i class="fa fa-phone"></i>&nbsp;9843268565 </li>';

            $('#userCourtBookedModal .user-profile').html(userProfile);
            var courtsBooked='';
            var start_time="";
            var end_time="";
            $.each(data.courtTimeSchedules,function(i,e){
                start_time=$self.timeConvert(e.time_schedule.start_hour);
                end_time=$self.timeConvert(e.time_schedule.end_hour);
                courtsBooked+='<tr>'+
                    '<td>'+(i+1)+'</td>'+
                    '<td>'+ e.court.court_name+'</td>'+
                    '<td>'+ start_time+'-'+end_time+'</td>'+
                    '<td>'+ e.date+'</td>'+
                    '<td>'+ e.time_schedule.price+'</td>'+

                    '</tr>';

            })
            $('#userCourtBookedModal .modal-title').text('Print Booked Courts');
            $('#userCourtBookedModal .printBookedCourtBtn').attr('data-id',data.id);
            $('#userCourtBookedModal #viewUserCourtBookedTable tbody').html(courtsBooked);
            $('#userCourtBookedModal #total-price').text(data.total_price);
            $('#userCourtBookedModal .bookedDate').html('<li><i class="fa fa-calendar"></i>&nbsp;'+data.updated_at);
            $('#ajax-loader-printCourt').hide();

        });

    }

    this.timeConvert=function(time){

        // Check correct time format and split into components
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        time=time.join ('').replace(':00:00','');
        return time;// return adjusted time or original string

    }



    this.init = function () {


        $self.bindFunction();

    }();

}

