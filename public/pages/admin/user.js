var user_module= new user();
function  user(){
    var $self=this;
    this.bindFunction=function(){
        $('#userCourtBookedTable').DataTable({
            processing: true,
        });
        $('#userCourtBookedTable .userBookingStatus').on('change',function(){
            var status=$(this).val();
            var id=$(this).data('id');
            var oldStatus = $(this).data('status');
            if(confirm('Are you sure want to '+status+' User Court Booking')){
               

                $.ajax({
                    url:base_url+'/admin/usersChangeStatus/'+id+'/'+status,
                    type:'GET',
                    success:function(data){
                        $(this).val(data.status);
                       
                        $('#success-msg-alert').show().find('strong').html('Successfully ! ' + data.success);
                        $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');
                    }
                });

            }else{
                $(this).val(oldStatus);

               
                return false;
            }


        });
        $('#userCourtBookedModal .userBookingStatus').on('click',function(){
            var status=$(this).data('status');
            var id=$(this).data('id');
            var changeStatus="";
            if(status=='Confirmed'){
                changeStatus='Not Confirmed';
            }else{
                changeStatus='Confirmed';
            }
            if(confirm('Are you sure want to '+changeStatus+' User Court Booking')){


                $.ajax({
                    url:base_url+'/admin/usersChangeStatus/'+id+'/'+status,
                    type:'GET',
                    success:function(data){
                        $('#userCourtBookedTable #status'+id).val(data.status);
                        $('#userCourtBookedModal').modal('hide');
                        $('#success-msg-alert').show().find('strong').html('Successfully ! ' + data.success);
                        $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');

                    }

                });
            }



        })

        $('#userCourtBookedTable .viewUserCourtBooked').on('click',function(){
            var id=$(this).data('id');
            $('#userCourtBookedModal').modal('show');



            $self.viewUserCourtsBooked(id);




        });
    }
    this.viewUserCourtsBooked=function(id){


        new ajaxHelper([base_url + "/admin/showUserCourtBooked/"+id, "", "get", ""]).makeAjaxCall().then(function (data) {
            var userProfile ='<li><i class="fa fa-user"></i>&nbsp;'+data.user.name+' </li>'+
                '<li> <i class="fa fa-envelope"></i>&nbsp;'+data.user.email+' </li>' +
                '<li> <i class="fa fa-building"></i>&nbsp;Inacho, Bhaktapur </li>'+
                '<li> <i class="fa fa-phone"></i>&nbsp;9843268565 </li>';

            $('#userCourtBookedModal .user-profile').html(userProfile);
            var courtsBooked='';
            var start_time="";
            var end_time="";
            $.each(data.courtTimeSchedules,function(i,e){
                start_time=$self.timeConvert(e.time_schedule.start_hour);
                end_time=$self.timeConvert(e.time_schedule.end_hour);
                courtsBooked+='<tr>'+
                    '<td>'+(i+1)+'</td>'+
                    '<td>'+ e.court.court_name+'</td>'+
                    '<td>'+ start_time+'-'+end_time+'</td>'+
                    '<td>'+ e.date+'</td>'+
                    '<td>'+ e.time_schedule.price+'</td>'+

                    '</tr>';

            })
            $('#userCourtBookedModal .modal-title').text('('+data.user.name+') Booked Courts');
            $('#userCourtBookedModal .userBookingStatus').attr('data-id',data.id);
            if(data.status=='Not_Confirmed'||data.status=='Pending'){
                $('#userCourtBookedModal .userBookingStatus').attr('data-status','Confirmed');
                $('#userCourtBookedModal .userBookingStatus').text('Confirmed').addClass('btn-success').removeClass('btn-danger');

            }else{
                $('#userCourtBookedModal .userBookingStatus').attr('data-status','Not_Confirmed');
                $('#userCourtBookedModal .userBookingStatus').text('Not Confirmed').addClass('btn-danger').removeClass('btn-success');

            }


            $('#userCourtBookedModal #viewUserCourtBookedTable tbody').html(courtsBooked);
            $('#userCourtBookedModal #total-price').text(data.total_price);
            $('#userCourtBookedModal .bookedDate').html('<li><i class="fa fa-calendar"></i>&nbsp;'+data.updated_at);


        });

    }
    this.timeConvert=function(time){

            // Check correct time format and split into components
            time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

            if (time.length > 1) { // If time format correct
                time = time.slice (1);  // Remove full string match value
                time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
                time[0] = +time[0] % 12 || 12; // Adjust hours
            }
        time=time.join ('').replace(':00:00','');
        return time;// return adjusted time or original string

    }
    this.init = function () {


        $self.bindFunction();

    }();
}

