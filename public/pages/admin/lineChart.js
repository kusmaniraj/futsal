// Line chart


if ($('#lineChart').length ){



    $.get(base_url + "/admin/dashboard/lineChart",function(data){
        makeLineChart(data);
    });
   function makeLineChart(reserveData){
       var ctx = document.getElementById("lineChart");
       var lineChart = new Chart(ctx, {
           type: 'line',
           data: {
               labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
               datasets: [{
                   label: " Reservation",
                   backgroundColor: "rgba(38, 185, 154, 0.31)",
                   borderColor: "rgba(38, 185, 154, 0.7)",
                   pointBorderColor: "rgba(38, 185, 154, 0.7)",
                   pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                   pointHoverBackgroundColor: "#fff",
                   pointHoverBorderColor: "rgba(220,220,220,1)",
                   pointBorderWidth: 1,
                   data: reserveData
               }, {
                   label: "Visit",
                   backgroundColor: "rgba(3, 88, 106, 0.3)",
                   borderColor: "rgba(3, 88, 106, 0.70)",
                   pointBorderColor: "rgba(3, 88, 106, 0.70)",
                   pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                   pointHoverBackgroundColor: "#fff",
                   pointHoverBorderColor: "rgba(151,187,205,1)",
                   pointBorderWidth: 1,
                   data: [82, 23, 66, 9, 99, 4, 2]
               }]
           },
       });
   }





}