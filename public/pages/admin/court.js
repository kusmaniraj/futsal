var cout_module = new court();
function court() {


    var $self = this;
    var courtDataTable="";
    this.intiateDataTable= function(){
        $self.courtDataTable=$('#courtTable').DataTable({
            processing: true,
        });
    }


    this.bindFunction = function (){
        $self.intiateDataTable();

        //show modal bind form
        //create bind
        $('#addCourtsBtn').on('click', function () {
            $('#courtModal').modal('show');

            $('label').closest('.form-group').removeClass('has-error');
            $('input').next('.error_msg').html('');

            $('#courtModal').find('input[type="text"],input[type="number"]').val('');
            $('#courtModal').find('button[type="submit"]').text('Create Court');
            $('#courtModal ').find('.modal-title').text('Create Futsal Court');


        });
        //delete bind
        $('#courtTable').on('click', '.delCourtBtn', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            $self.deleteCourt(id);


        });
        //  submit bind

        $('#courtForm').on('submit', function (e) {
            e.preventDefault();
            var id=$('#courtModal #courtForm').find('input[name="id"]').val();
            if(id==""){
                $self.createCourt();
            }else{
                $self.updateCourt(id);
            }




        });

        //edit bind
        $('#courtTable').on('click', '.editCourtBtn', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $('#courtModal').modal('show');
            $('label').closest('.form-group').removeClass('has-error');
            $('input').next('.error_msg').html('');

            $self.editCourt(id);


        });






    }

    //end bind function
    this.createCourt = function () {
        var formData = $('#courtForm').serialize();
        new ajaxHelper([base_url + "/admin/court", formData, "post", ""]).makeAjaxCall().then(function (data) {




            //append row
            var countRow = $('#courtTable').find('tbody tr').length;
            var row = '<tr id="court'+ data.id +'" data-id="'+data.id+'">' +
                '<td id="key'+data.id+'" data-id="'+(countRow + 1)+'">' + (countRow + 1) + '</td>' +
                '<td>' + data.court_name + '</td>' +


                '<td>' +
                '<a  data-id="' + data.id + '" id="" class="btn btn-primary editCourtBtn"><i class="fa fa-edit"></i></a>' +
                '<a data-id="' + data.id + '" class="btn btn-danger  delCourtBtn " id=""><i class="fa fa-remove"></i></a>' +
                '</td>' +
                '</tr>';
            $self.courtDataTable.destroy();
            $('#courtTable').find('tbody').append(row);
            $self.intiateDataTable();

            $('#courtModal').modal('hide');

            $('#success-msg-alert').show().find('strong').html('Successfully' + data.success);
            $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');


        }, function (err) {
            $('.error_msg').removeClass('hidden');

            var data = JSON.parse(err.responseText);

            $.each(data.errors, function (i, value) {
                $('label[for="' + i + '"]').closest('.form-group').addClass('has-error');
                $('input[name="' + i + '"]').next('.error_msg').html('<span class="help-block">' +
                    '<strong>' + value + '</strong>' +
                    '</span>');
            })
        });
    }

    //edit Court
    this.editCourt=function(id){
        new ajaxHelper([base_url + "/admin/court" + "/" + id+"/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
           $('#courtModal #courtForm').find('input[name="court_name"]').val(data.court_name);
            $('#courtModal #courtForm').find('input[name="court_price"]').val(data.court_price);
            $('#courtModal #courtForm').find('input[name="id"]').val(data.id);
            $('#courtModal').find('button[type="submit"]').text('Update Court');
            $('#courtModal').find('.modal-title').text('Update Futsal Court');



        });
    }
    //update
    this.updateCourt = function (id) {
        var formData = $('#courtForm').serialize()+ '&_method='+'PUT';

        new ajaxHelper([base_url + "/admin/court"+"/"+id, formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.error){
                alert(data.error);
            }else{

                var key =$('#courtTable').find('#key'+data.id).data('id');
               
                var row ='<td>' + (key ) + '</td>' +
                    '<td>' + data.court_name + '</td>' +


                    '<td>' +
                    '<a  data-id="' + data.id + '" id="" class="btn btn-primary editCourtBtn"><i class="fa fa-edit"></i></a>' +
                    '<a data-id="' + data.id + '" class="btn btn-danger  delCourtBtn " id=""><i class="fa fa-remove"></i></a>' +
                    '</td>';

                $('#courtTable').find('#court'+data.id).html('');
                $('#courtTable').find('#court'+data.id).append(row);

                $('#courtModal').modal('hide');

                $('#success-msg-alert').show().find('strong').html('Successfully !' + data.success);
                $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');
            }




            //append row



        }, function (err) {
            $('.error_msg').removeClass('hidden');

            var data = JSON.parse(err.responseText);

            $.each(data.errors, function (i, value) {
                $('label[for="' + i + '"]').closest('.form-group').addClass('has-error');
                $('input[name="' + i + '"]').next('.error_msg').html('<span class="help-block">' +
                    '<strong>' + value + '</strong>' +
                    '</span>');
            })
        });
    }

    //delete court
    this.deleteCourt = function (id) {
        var formData = {_token: $('meta[name="csrf-token"]').attr('content')};
        if(confirm('Are you sure want to delete ?')){
            new ajaxHelper([base_url + "/admin/court" + "/" + id, formData, "delete", ""]).makeAjaxCall().then(function (data) {
                if (data.error) {


                    $('#error-msg-alert').show().find('strong').html('Error! ' + data.error);
                    $('#error-msg-alert').slideDown('slow').delay(1500).slideUp('slow');
                } else {
                    $self.courtDataTable.destroy();
                    $('#courtTable #court' + id).remove();
                    $("#courtTable tbody  tr").each(function (key) {
                        $(this).find("td:first").text(key+1);

                    });
                    $self.intiateDataTable();
                    $('#success-msg-alert').show().find('strong').html('Successfully ! ' + data.success);
                    $('#success-msg-alert').slideDown('slow').delay(1500).slideUp('slow');
                }

            });
        }else{
            return false;
        }

    }


    this.init = function () {


        $self.bindFunction();

    }();

}

