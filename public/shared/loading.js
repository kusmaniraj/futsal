$('<div id="loading"/>').css({

    width: $(window).width() + 'px',
    height: $(window).height() + 'px',

}).hide().appendTo('body');

$(document)
    .ajaxStart(function(){
      $("#loading").show();

    })
    .ajaxStop(function(){
      $("#loading").hide();
    });